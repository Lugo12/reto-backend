import ajax from "./ajax.js";   //importando la función aja
import {eye, hideErrors, showErrors} from "./stuff.js";   //importando la libreria stuff
//Función autoinvocable
(function(){
    //Elemntos del DOM
    const $form = document.getElementById('form');    
    //Delegación del evento submit
    $form.addEventListener('submit', e => {
        //Detenemos el envio de datos
        e.preventDefault();
        //Constantes con los valores de los campos
        const $email = document.getElementById('email').value,
            $password = document.getElementById('password').value;
        //Ocultamos errores por default
        hideErrors();  
        //Validación de campos
        try {
            if (!$email || !$password || $email.trim() == '' || $password.trim() == '') 
                throw "Porfavor llena los campos.";
            //Construimos la petición en formato JSON:API
            const request = {
                data:{
                    type:"users",
                    attributes:{
                        email:$email,
                        password:$password
                    }
                }
            }
            //Enviamos los datos al servidor para validar la informción
            login(JSON.stringify(request));
        } catch (error) {
            showErrors(error);
        }        
    });
    //Realizamos la petición al servidor
    const login = (request) => ajax(
        'POST',
        'http://localhost:1000/api/v1/login',
        request,
        data => {               
            //Guardamos el token en el LocalStorage
            localStorage.setItem('token', data.data.attributes.token);
            //Redirigimos al usuario al index
            window.location.href = '/';
        },
        error => {
            //Error en consol
            console.error(error);
            //Error en el login
            showErrors(error.errors[0].detail);
        }
    );
    //Delegación del evento click
    document.addEventListener('click', e => {
        const $eye = document.querySelector('.ojo');
        if (e.target === $eye)
            eye((e.target.classList.contains('fa-eye')) ? 'fa-eye' : 'fa-eye-slash');            
    });
})();