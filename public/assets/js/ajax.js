//funcion para todas las peticiones ajax al servidor con API fetch
export default async (
    method, 
    url, 
    body = null, 
    success, 
    reject,
	headers = {'Content-type': 'application/vnd.api+json'}
) => {
  	try {
    	//atributos de la peticion
    	let options = {
    	    method,
    	    headers: headers,
    	    body
    	},
    	res = await fetch(url, options),      //Peticion
    	data = await res.json();      //Recuperando la data
    	//En caso de que haya fallado la petición se envia al catch
    	if (!res.ok) throw data;
    	success(data);    //funcion en caso de exito
  	} catch (err) {    
    	reject(err);    //funcion en caso de fracaso
  	}
};
