//Función para mostrar errores
export const showSuccess = message => {
    const $success = document.querySelector('.success');
    $success.textContent = message;
    $success.classList.remove('oculto');
};
//Función para mostrar errores
export const showErrors = message => {
    const $errors = document.querySelectorAll('.errores');
    $errors.forEach(el => {
        if (el.classList.contains('error-message')) el.textContent = message;
        el.classList.remove('oculto');
    });
};
//Función para mostrar errores
export const hideErrors = () => {
    const $errors = document.querySelectorAll('.errores');
    $errors.forEach(el => {
        if (el.classList.contains('error-message')) el.textContent = '';
        el.classList.add('oculto');
    });
};
//Funcion para activar el ojo
const showPass = () => {
    document.getElementById('password').setAttribute('type', 'text');
    document.querySelector('.ojo').classList.remove('fa-eye');
    document.querySelector('.ojo').classList.add('fa-eye-slash');
};
//Funcion para desactivar el ojo
const hidePass = () => {
    document.getElementById('password').setAttribute('type', 'password');
    document.querySelector('.ojo').classList.remove('fa-eye-slash');
    document.querySelector('.ojo').classList.add('fa-eye');
};
//Función para el click del ojo
export const eye = clase => {
    if (clase === 'fa-eye') showPass();
    else if (clase === 'fa-eye-slash') hidePass();
}