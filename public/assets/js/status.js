import ajax from "./ajax.js";   //importando la función aja
//Función autoinvocable
(function(){
    //Elemnto del DOM donde se alojara nuestro evento
    const $main = document.querySelector('main');
    //Recuperamos el status de la ruta
    const status = window.location.toString().split('/')[3];
    //Realizamos la petición al servidor
    const request = (page = null) => ajax(
        'GET',
        `http://localhost:1000/api/v1/event?status=${
            (status == 'proximos') 
            ? 'próximo' 
            : (status == 'pasados') 
            ? 'pasado' : 'oferta'}${(page) ? page : '' }`,
        null,
        data => {
            //Reiniciamos el main
            $main.innerHTML = '';               
            //Contenedor de eventos
            const $contenedor = document.createElement('div');
            $contenedor.classList.add('contenedor_eventos');
            //Se pinta el evento
            data.data.forEach(event => {
                //Atributos del evento
                const attributes = event.attributes;
                $contenedor.innerHTML += `
                    <div class="event" data-id="${event.id}">
                        <div class="event_image">
                            <img src="assets/img/${attributes.poster}" alt="<image lost>">
                        </div>
                        <div class="event_details">
                            <p> ${attributes.place} </p>
                            <h4> ${attributes.name} </h4>
                        </div>
                    </div>
                `;
            });
            //Agregamos el contenedor de eventos al main
            $main.appendChild($contenedor);
            //Si hay más de 10 eventos agregamos el paginador
            if (data.meta.page.total > 10) {
                //Página actual
                const currentPage = data.meta.page["current-page"];
                //Ultima página
                const lastPage = data.meta.page["last-page"];
                //Paginador
                const $paginador = document.createElement('nav');
                $paginador.classList.add('navigation');
                $paginador.innerHTML = `
                    <ul class="pagination">
                        <li class="page-item ${(currentPage === 1) ? 'disabled' : ''}">
                            <button class="page-link prev">Anterior</button>
                        </li>                        
                        <li class="page-item ${(currentPage === lastPage) ? 'disabled' : ''}">
                            <button class="page-link next">Siguiente</button>
                        </li>
                    </ul>
                `;
                $paginador.dataset.currentPage = currentPage;
                //Agregando items al paginador
                for (let i = 1; i <= lastPage; i++) {
                    //Creamos un item por página
                    let $item = document.createElement('li');
                    $item.classList.add('page-item');
                    //Agregamos o removemos la clase active según el caso
                    (currentPage === i) ? $item.classList.add('active') : $item.classList.remove('active');
                    //Terminamos de construir el item
                    $item.innerHTML = `<button class="page-link item">${i}</button>`;
                    $item.dataset.page = i; 
                    //Agregamos el item al paginador
                    $paginador.children[0].insertBefore($item, $paginador.children[0].lastElementChild);
                }
                $main.appendChild($paginador);
            }            
        },
        error => {
            console.error(error);
            $main.innerHTML = `<h2 class="errors">${error.errors[0].detail}</h2>`;
        }
    );
    //Delegación del evento click
    document.addEventListener('click', e => {
        const $items = Array.from(document.querySelectorAll('.item'));
        const $next = document.querySelector('.next');
        const $prev = document.querySelector('.prev');
        //Si coincide con los target esperados realizamos la debida acción
        if ($items.includes(e.target)) {
            /* Si el target es algun botón de una página que no sea activa
            una nueva petición con la página especificada */
            (!e.target.parentNode.classList.contains('active')) 
            ? request(`&page=${e.target.parentNode.dataset.page}`) : null;
        } else if (e.target === $next) {
            //Petición a la página siguiente
            request(`&page=${parseInt(e.target.parentNode.parentNode.parentNode.dataset.currentPage)+1}`);
        } else if (e.target === $prev) {
            //Petición a la página anterior
            request(`&page=${parseInt(e.target.parentNode.parentNode.parentNode.dataset.currentPage)-1}`);
        }
    });
    //Realizamoos la petición al cargar la página
    request();
})();