import ajax from "./ajax.js";   //importando la función aja
import {eye, hideErrors, showErrors, showSuccess} from "./stuff.js";   //importando la libreria stuff
//Función autoinvocable
(function(){
    //Elemntos del DOM
    const $form = document.getElementById('form');    
    //Delegación del evento submit
    $form.addEventListener('submit', e => {
        //Detenemos el envio de datos
        e.preventDefault();
        //Constantes con los valores de los campos
        const $user = document.getElementById('user').value,
            $email = document.getElementById('email').value,
            $password = document.getElementById('password').value;
        //Ocultamos errores por default
        hideErrors();  
        //Validación de campos
        try {
            if (
                !$user 
                || !$email 
                || !$password 
                || $email.trim() == '' 
                || $user.trim() == '' 
                || $password.trim() == ''
            ) throw "Porfavor llena los campos.";
            //Expresiones regulares
            const expresiones = {
                user: /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/,
                email: /^[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,5}$/,
                password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,16}$/
            };
            //Validar expresiones
            if (!expresiones.user.test($user)) 
                throw 'El usuario permite letras, números, guión medio y punto, longitud de mínimo 8, máximo 20.';
            if (!expresiones.email.test($email)) 
                throw 'Porfavor ingresa un correo valido.';
            if (!expresiones.password.test($password)) 
                throw 'En la contraseña al menos una minúscula, una mayúscula, un número, un caracter especial, longitud de mínimo 8, máximo 16.';            
            //Construimos la petición en formato JSON:API
            const request = {
                data:{
                    type:"users",
                    attributes:{
                        user:$user,
                        email:$email,
                        password:$password
                    }
                }
            }
            //Enviamos los datos al servidor para validar la informción
            registro(JSON.stringify(request));
        } catch (error) {
            showErrors(error);
        }        
    });
    //Realizamos la petición al servidor
    const registro = (request) => ajax(
        'POST',
        'http://localhost:1000/api/v1/register',
        request,
        data => {               
            //Mensaje de éxito
            showSuccess(`Hola ${data.data.attributes.user}! Tu cuenta se creó con éxito! Serás redireccionado en un momento!`);
            //Redirigimos al usuario al login
            setTimeout(() => window.location.href = '/sign-in', 4000);
        },
        error => {
            //Error en consol
            console.error(error);
            //Error en el login
            showErrors(error.errors[0].detail);
        }
    );
    //Delegación del evento click
    document.addEventListener('click', e => {
        const $eye = document.querySelector('.ojo');
        if (e.target === $eye)
            eye((e.target.classList.contains('fa-eye')) ? 'fa-eye' : 'fa-eye-slash');            
    });
})();