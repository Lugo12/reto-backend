import ajax from "./ajax.js";   //importando la función aja
(function(){
    //Validación para el retroceso de la página
    window.onbeforeunload = function(e) {
        console.log("Validando cliente en la página");
    };
    //Delegación del evento click
    document.addEventListener('click', e => {
        const $events = Array.from(document.querySelectorAll('.event'));
        if ($events.includes(e.target))
            //Detalles de los eventos
            window.location.href = `/detalles/${e.target.dataset.id}`;
        else if (e.target === document.querySelector('.logout')) {
            //Cerrar Sesión
            localStorage.clear();
            window.location.href = '/';
        }
    });
    //Comprobación de usuario logueado
    const token = localStorage.getItem('token');
    //Elementos del DOM
    const $header = document.querySelector('header');
    if (token) {
        //Validamos el token
        ajax(
            'POST',
            'http://localhost:1000/api/v1/token',
            JSON.stringify({"token":token}),
            data => {                
                //Si el token es válido
                if (data.is_valid) {
                    $header.firstElementChild.insertAdjacentHTML('afterend',`
                    <div class="icon"><a href="/admin"><i class="fa-solid fa-user-tie"></i><span>Admin</span></a></div>
                    <div class="icon"><a class="logout" href="#"><i class="fa-solid fa-right-from-bracket"></i><span>Logout</span></a></div>
                    `);
                } else{
                    //Mostramos el botón de login
                    localStorage.clear();
                    $header.firstElementChild.insertAdjacentHTML('afterend',`<div class="icon"><a href="/sign-in"><i class="fa-solid fa-right-to-bracket"></i><span>Login</span></a></div>`);
                }
            },
            error => {console.log(error);}
        );
    } else {
        //Mostramos el botón de login
        $header.firstElementChild.insertAdjacentHTML('afterend',`<div class="icon"><a href="/sign-in"><i class="fa-solid fa-right-to-bracket"></i><span>Login</span></a></div>`);
    }
})();