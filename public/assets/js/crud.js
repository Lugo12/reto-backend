import ajax from "./ajax.js";   //importando la función aja
import alert from "./alert.js";   //importando la función aja
//Función autoinvocable
(function(){
    //Validación para el retroceso de la página
    window.onbeforeunload = function(e) {
        console.log("Validando cliente en la página");
    };
    //Comprobación de usuario logueado
    const token = localStorage.getItem('token');    
    if (token) {
        //Validamos el token
        ajax(
            'POST',
            'http://localhost:1000/api/v1/token',
            JSON.stringify({"token":token}),
            data => {                
                //Si el token es válido
                if (data.is_valid) {
                    document.getElementById('loader').classList.add('oculto');
                    admin();
                }
                else
                    //Mandamamos al cliente al login
                    window.location.href = '/sign-in';
            },
            error => {console.log(error);}
        );
    } else
        //Mandamamos al cliente al login
        window.location.href = '/sign-in';
    //Función en caso de que se entre a la sección de admin
    const admin = () => {
        const $main = document.querySelector('.main'),
            $allevents = document.querySelector('.allEvents'),
            $addevents = document.querySelector('.addEvents'),
            $modal = document.getElementById('modal');
        //const $btnsEdit = Array.from(document.querySelectorAll('.btn-edit'));
        //Delegación del evento click
        document.addEventListener('click', e => {
            //Botones para borrar
            const $btnsDelete = Array.from(document.querySelectorAll('.btn-delete'));
            //Botones para actualizar
            const $btnsEdit = Array.from(document.querySelectorAll('.btn-edit')),
                $btnEdit = document.querySelector('.btn-actualizar');
            if (e.target === $addevents) {
                //Mostrando la sección de agregar eventos en la interfz del admin
                $addevents.classList.add('active');
                $allevents.classList.remove('active');
                addEvents();
            } else if (e.target === $allevents) {
                //Mostrando la sección de eventos en la interfaz del admin
                $addevents.classList.remove('active');
                $allevents.classList.add('active');
                allEvents();
            } else if ($btnsDelete.includes(e.target)) {
                //Petición para borrar eventos                                
                const id = e.target.dataset.id;
                //Confirmación de borrado
                const $confirm = confirm(`¿Está seguro de borrar permanentemente el evento con id: ${id}?`);
                if ($confirm) {
                    //Petición para borrar el evento
                    (async () => {
                        let options = {
                            method: 'DELETE',
                            headers: {
                                'Content-type': 'application/vnd.api+json',
                                'Authorization': localStorage.getItem('token')
                            }
                        },
                        //Peticion
                        res = await fetch(`http://localhost:1000/api/v1/event/${id}`, options);
                        if (res.ok) {
                            allEvents(); 
                            alert('Hecho!', 'El evento se borró satisfactoriamente.', 'success');
                        }
                        else {
                            //Si hay un error
                            const error = await res.json();
                            console.log(error);
                            alert('Algo salió mal', error.errors[0].detail, 'danger');
                        }
                    })();
                }
            } else if ($btnsEdit.includes(e.target)) {
                //Actualizar evento
                $modal.classList.add('modal-active');
                //Llenamos el modal con el formulario
                $modal.firstElementChild.children[1].innerHTML = ` 
                    <div class="modal-event">
                        <form id="form_update">
                            <div class="form">
                                <div class="content_div">
                                    <label for="name">Nombre del evento</label>
                                    <input class="event_input_obligatorio" required="required" type="text" id="name">
                                </div>
                                <div class="content_div">
                                    <label for="date">Fecha y hora</label>
                                    <div>
                                        <input class="event_input_obligatorio" required="required" type="date" id="date">
                                        <input class="event_input_obligatorio" required="required" type="time" id="time">
                                    </div>
                                </div>
                                <div class="content_div">
                                    <label for="city">Ciudad</label>
                                    <input class="event_input_obligatorio" required="required" type="text" id="city">
                                </div>
                                <div class="content_div">
                                    <label for="place">Lugar</label>
                                    <input class="event_input_obligatorio" required="required" type="text" id="place">
                                </div>
                                <div class="content_div">
                                    <label for="ticket_limit">Límite de tickets</label>
                                    <input class="event_input_obligatorio" required="required" min="1" max="255" step="number" type="number" id="ticket_limit">
                                </div>
                                <div class="content_div">
                                    <label for="minium_age_to_pay">Edad mínima para pagar</label>
                                    <input class="event_input_obligatorio" required="required" min="1" max="255" type="number"
                                    id="minium_age_to_pay">
                                </div>
                                <textarea class="event_input_obligatorio" required="required" id="information"></textarea>
                                <div class="content_div">
                                    <label for="poster">Poster del evento</label>
                                    <input class="event_input_obligatorio" required="required" type="file" id="poster">
                                </div>
                                <div class="content_div">
                                    <label for="price">Precio del ticket $</label>
                                    <input class="event_input_obligatorio"
                                    required="required" min="1" max="999999" step="0.01" type="number"
                                    id="price">
                                </div>
                                <div class="content_div">
                                    <label for="approximate_duration">Duración aproximada</label>
                                    <input class="event_input_opcional" type="text"
                                    id="approximate_duration" placeholder="ej: 2 Horas">
                                </div>
                                <div class="content_div">
                                    <label for="age_limit">Límite de edad</label>
                                    <input class="event_input_opcional" type="text" id="age_limit"
                                    placeholder="ej: Mayores de 18 años">
                                </div>
                                <div class="content_div">
                                    <label for="restrictions">Restricciones</label>
                                    <input class="event_input_opcional" type="text" id="restrictions"
                                    placeholder="ej: Cámara sin flash">
                                </div>
                                <div class="content_div">
                                    <label for="services">Servicios</label>
                                    <input class="event_input_opcional" type="text" id="services"
                                    placeholder="ej: comida y bebida">
                                </div>
                                <div class="content_div">
                                    <label for="official_website">Sitio web oficial</label>
                                    <input class="event_input_opcional" type="text" id="official_website"
                                    placeholder="ej: www.misitio.com">
                                </div>
                                <div class="content_div">
                                    <label for="discount">Descuento (ej: 10%)</label>
                                    <div>
                                        <input class="event_input_opcional" type="range" min="0" max="99"
                                        id="discount" value="0" oninput="this.nextElementSibling.value =
                                        this.value">
                                        <output id="output_discount">0</output>
                                    </div>                        
                                </div>                    
                            </div>
                        </form>
                    </div>
                `;
                //Petición para traer la información del evento
                ajax(
                    'GET',
                    `http://localhost:1000/api/v1/event/${e.target.dataset.id}`,
                    null,
                    data => {               
                        //Traemos los inputs del formulario
                        let $inputs = Array.from(document.querySelectorAll('.event_input_obligatorio'));
                        $inputs = $inputs.concat(Array.from(document.querySelectorAll('.event_input_opcional'))); 
                        //Atributos del evento
                        const attributes = data.data.attributes;
                        //Rellenamos los inputs
                        $inputs.forEach(el => {
                             if (
                                 attributes[el.id] 
                                 && el.id !== 'date'
                                 && el.id !== 'poster'
                                 && el.id !== 'discount'
                            )
                                el.value = data.data.attributes[el.id]
                        });
                        //Atributos individuales
                        //Fecha y hora
                        document.getElementById('date').value = attributes.date.substring(0, 10);
                        document.getElementById('time').value =  attributes.date.substring(11, 16);                                                
                        //Descuento
                        if(attributes.discount) {
                            const descuento = attributes.discount.substring(0, attributes.discount.length-1); 
                            document.getElementById('discount').value = descuento;
                            document.getElementById('output_discount').textContent = descuento;
                        }
                        //Asignamos el id al botón de editar principal
                        $btnEdit.dataset.id = data.data.id;
                    },
                    error => console.error(error)
                );
            } else if (e.target === $btnEdit) {
                //Botón para actualizar evento específico                
                const $obligatorios = Array.from(document.querySelectorAll('.event_input_obligatorio')),
                    $opcionales = Array.from(document.querySelectorAll('.event_input_opcional'));
                //Reiniciamos los border rojos en los inputs
                $obligatorios.forEach(el => el.classList.remove('wrong_input'));
                //Bandera de errores
                let flag = false;
                try {
                    //Validación de campos con solo espacios
                    $obligatorios.forEach(el => {
                        if (el.value.trim() === "" && el.id !== "poster") {
                            el.classList.add('wrong_input');
                            flag = true;
                        }
                    });
                    //Cuando los campos no estan llenados correctamente mandamos excepción
                    if (flag) throw { message:"Campos invalidos", type: "danger"}
                    //Id del evento
                    const id = $btnEdit.dataset.id;                                        
                    //Si hubo cambios lo almacenamos en los atributos
                    ajax(
                        'GET',
                        `http://localhost:1000/api/v1/event/${id}`,
                        null,
                        data => {
                            //Atributos del evento en la petición
                            const attributes = data.data.attributes;
                            //Comparamos valores y asignarlos a un nuevo arreglo
                            const updObligatorios = $obligatorios.filter(el => {
                                return el.value !== attributes[el.id];
                            });
                            //Datos en texto
                            const json = {
                                data:{
                                    type:"events",
                                    id: id,
                                    attributes:{}
                                }
                            }
                            //Cuerpo de la petición
                            const request = new FormData();
                            //Si hay cambios los mandamos dentro del json
                            updObligatorios.forEach(el => {
                                if (
                                    el.id !== 'date'
                                    && el.id !== 'time'
                                    && el.id !== 'poster'
                                ) {
                                    json['data']['attributes'][el.id] = el.value;
                                }
                            });
                            //Validamos la fecha
                            const fecha = document.getElementById('date').value + ' ' + document.getElementById('time').value;
                            if (fecha !== attributes['date'].substring(0, 16)) {
                                json['data']['attributes']['date'] = fecha;
                            }
                            //Validando campos opcionales
                            const updOpcionales = $opcionales.filter(el => {
                                return el.value 
                                    && el.value.trim() !== '' 
                                    && el.value !== attributes[el.id];
                            });
                            //Si hay cambios los mandamos dentro del json
                            updOpcionales.forEach(el => {
                                if (el.id !== 'discount') {
                                    json['data']['attributes'][el.id] = el.value;
                                }
                            });
                            //Validando el descuento
                            const descuento = document.getElementById('discount').value;
                            if (
                                attributes['discount']
                                && descuento == 0
                            ) {
                                //Validando el descuento cuando este se retira
                                json['data']['attributes']['status'] = 'próximo';
                            } else if (
                                (attributes['discount']
                                && descuento > 0 
                                && descuento+"%" !== attributes['discount'])
                                || (!attributes['discount']
                                && descuento > 0 ) 
                            ) {
                                //Validando el descuento cuando se modifica o se agrega
                                json['data']['attributes']['discount'] = descuento+"%";
                            }
                            //Aqui es donde se valida si realmente hubo cambios
                            if (Object.values(json['data']['attributes']).length > 0) {
                                request.append('json', JSON.stringify(json));
                            }
                            //Si se cambio la imagen, la mandamos     
                            if(document.getElementById('poster').files[0]) {
                                request.append('image', document.getElementById('poster').files[0]);
                            }
                            /* Validación para realizar o no la petición para
                            el evento */
                            if (request.has('json') || request.has('image')) {
                                //Petición para borrar el evento
                                (async () => {
                                    let options = {
                                        method: 'POST',
                                        body: request,
                                        headers: {
                                            'Authorization': localStorage.getItem('token')
                                        }
                                    },
                                    //Peticion
                                    res = await fetch(`http://localhost:1000/api/v1/event/${id}`, options);
                                    if (res.ok) {
                                        //Evento actualizado
                                        alert("Hecho!", `El evento actualizdo con éxito`, "success");
                                        //Renderizamos los eventos
                                        allEvents();
                                        //Cerramos el modal de eventos
                                        $modal.classList.remove('modal-active');
                                    }
                                    else {
                                        //Si hay un error
                                        const error = await res.json();
                                        console.log(error);
                                        alert('Error', error.errors[0].detail, 'danger');
                                    }
                                })();
                            } else {
                                //Cerramos el modal de eventos
                                $modal.classList.remove('modal-active');
                                alert('Información', 'No se realizo ningún cambio', 'info');
                            }
                        },
                        error => {
                            console.error(error)
                            alert('Algo salio mal', 'Intentalo de nuevo más tarde', 'danger');
                        } 
                    );                    
                } catch (error) {
                    alert('Error', error.message, error.type)   
                }
            } else if (e.target === $modal || e.target === document.querySelector('.btn-cancelar')) {
                //Cerrar modal
                $modal.classList.remove('modal-active');
            } else if (e.target === document.querySelector('.logout')) {
                //Cerrar Sesión
                localStorage.clear();
                window.location.href = '/';
            }
        });
        //Delegación del evento keydown 
        document.addEventListener("keydown", e => {
            if (e.code === 'Escape' && $modal.classList.contains('modal-active'))
                $modal.classList.remove('modal-active');
        });
        //Petición para mostrar todos los eventos
        const allEvents = () => ajax(
            'GET',
            'http://localhost:1000/api/v1/event?include=user',
            null,
            async data => {
                //Si existen eventos que mostrar
                if (data.meta.page.total > 0) {
                    //Reiniciamos el main
                    $main.innerHTML = '';
                    //Contenedor de eventos
                    const $all = document.createElement('div');
                    $all.classList.add('all');
                    //Recogemos los eventos de todas las páginas
                    const getEventos = async () => {
                        //Variable donde vienen todos los eventos
                        let eventos = data.data,
                            users = data.included;
                        //Por cada página devolvemos los eventos que contienen
                        for (let i = 2; i <= data.meta.page["last-page"]; i++) {
                            await ajax(
                                'GET',
                                `http://localhost:1000/api/v1/event?include=user&page=${i}`,
                                null,
                                data => {
                                    eventos = eventos.concat(data.data)
                                    users = users.concat(data.included)
                                },
                                error => console.log(error)
                            )
                        }
                        //Retornamos todos los eventos en todas las páginas
                        return {eventos, users};
                    };
                    //Tabla donde se alojaran los eventos
                    const $table = document.createElement('table');
                    $table.classList.add('table', 'table-dark');
                    $table.innerHTML = `
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Fecha</th>
                                <th>Lugar</th>
                                <th>Poster</th>
                                <th>Precio</th>
                                <th>Estado</th>
                                <th>Creador</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    `;
                    //Esperamos a que la petición nos traiga todos los eventos
                    const eventos = await (await getEventos()).eventos;
                    const users = await (await getEventos()).users;
                    //Llenamos la tabla con los eventos
                    eventos.forEach(event => {
                        const $event = document.createElement('tr');
                        let $user = null;
                        //Encontramos el usuario encargado del evento
                        for (const user of users) {
                            if (event.attributes.user_id === user.id) {
                                $user = user.attributes.user;
                                break;
                            } 
                        }
                        //Fila   
                        $event.innerHTML = `
                            <td>${event.id}</td>
                            <td>${event.attributes.name}</td>
                            <td>${event.attributes.date}</td>
                            <td>${event.attributes.place}</td>
                            <td class="img"><img src="assets/img/${event.attributes.poster}" alt="<image fail>"></td>
                            <td>$${event.attributes.price}</td>
                            <td>${event.attributes.status}</td>
                            <td>${$user}</td>                        
                            <td class="acciones">
                                <button class="btn-edit" data-id= ${event.id}><i class="fa-solid fa-pen-to-square"></i></button>
                                <button class="btn-delete" data-id= ${event.id}><i class="fa-solid fa-trash-can"></i></button>
                            </td>
                        `;    
                        //Agregamos la fila a la tabla               
                        $table.lastElementChild.appendChild($event);             
                    });
                    //Agregamos la tabla al contenedor y el contenedor al main
                    $all.appendChild($table);
                    $main.appendChild($all);
                } else alert('Sin eventos', 'No hay eventos actualmente', 'info');
            },
            error => console.log(error)
        );
        //Función para mostrar el formularo en el main
        const addEvents = () => {
            //Reiniciamos el main
            $main.innerHTML = '';
            //Contenedor del formulario
            const $add = document.createElement('div');
            $add.classList.add('add');
            $add.innerHTML = `
                <h2>Nuevo evento</h2>
                <form id="form_create">
                    <h3>Obligatorio:</h3>          
                    <div class="form form_obligatorio">
                        <div class="content_div">
                            <label for="name">Nombre del evento</label>
                            <input class="event_input_obligatorio" required="required" type="text" id="name"
                            placeholder="ej: Mi evento">
                        </div>
                        <div class="content_div">
                            <label for="date">Fecha y hora</label>
                            <div>
                                <input class="event_input_obligatorio" required="required" type="date" id="date">
                                <input class="event_input_obligatorio" required="required" type="time" id="time">
                            </div>
                        </div>
                        <div class="content_div">
                            <label for="city">Ciudad</label>
                            <input class="event_input_obligatorio" required="required" type="text" id="city"
                            placeholder="ej: Ciudad de México">
                        </div>
                        <div class="content_div">
                            <label for="place">Lugar</label>
                            <input class="event_input_obligatorio" required="required" type="text" id="place"
                            placeholder="ej: Auditorio nacional">
                        </div>
                        <div class="content_div">
                            <label for="ticket_limit">Límite de tickets</label>
                            <input class="event_input_obligatorio" required="required" min="1" max="255" step="number" type="number" id="ticket_limit" placeholder="ej: 8">
                        </div>
                        <div class="content_div">
                            <label for="minium_age_to_pay">Edad mínima para pagar</label>
                            <input class="event_input_obligatorio" required="required" min="1" max="255" type="number"
                            id="minium_age_to_pay" placeholder="ej: 4">
                        </div>
                        <textarea class="event_input_obligatorio" required="required" id="information" placeholder="Información"></textarea>
                        <div class="content_div">
                            <label for="poster">Poster del evento</label>
                            <input class="event_input_obligatorio" required="required" type="file" id="poster">
                        </div>
                        <div class="content_div">
                            <label for="price">Precio del ticket $</label>
                            <input class="event_input_obligatorio"
                            required="required" min="1" max="999999" step="0.01" type="number"
                            id="price"
                            placeholder="ej: 500.00">
                        </div>
                    </div>      
                    <h3>Opcional:</h3>          
                    <div class="form form_opcional">
                        <div class="content_div">
                            <label for="approximate_duration">Duración aproximada</label>
                            <input class="event_input_opcional" type="text"
                            id="approximate_duration" placeholder="ej: 2 Horas">
                        </div>
                        <div class="content_div">
                            <label for="age_limit">Límite de edad</label>
                            <input class="event_input_opcional" type="text" id="age_limit"
                            placeholder="ej: Mayores de 18 años">
                        </div>
                        <div class="content_div">
                            <label for="restrictions">Restricciones</label>
                            <input class="event_input_opcional" type="text" id="restrictions"
                            placeholder="ej: Cámara sin flash">
                        </div>
                        <div class="content_div">
                            <label for="services">Servicios</label>
                            <input class="event_input_opcional" type="text" id="services"
                            placeholder="ej: comida y bebida">
                        </div>
                        <div class="content_div">
                            <label for="official_website">Sitio web oficial</label>
                            <input class="event_input_opcional" type="text" id="official_website"
                            placeholder="ej: www.misitio.com">
                        </div>
                        <div class="content_div">
                            <label for="discount">Descuento (ej: 10%)</label>
                            <div>
                                <input class="event_input_opcional" type="range" min="0" max="99"
                                id="discount" value="0" oninput="this.nextElementSibling.value =
                                this.value">
                                <output>0</output>
                            </div>                        
                        </div>                    
                    </div>
                    <input class="btn-submit" value="Agregar" type="submit"/>
                </form>
            `;
            $main.appendChild($add);
        }
        //Delegación del evento submit
        document.addEventListener('submit', e => {
            //Formularios
            const $form_create = document.getElementById('form_create');
            //Si el envio de datos...
            if (e.target === $form_create) {
                //Crear evento
                e.preventDefault();
                //Recogemos los inputs
                const $obligatorios = Array.from(document.querySelectorAll('.event_input_obligatorio')),
                    $opcionales = Array.from(document.querySelectorAll('.event_input_opcional'));
                //Reiniciamos los border rojos en los inputs
                $obligatorios.forEach(el => el.classList.remove('wrong_input'));
                //Bandera de errores
                let flag = false;
                try {
                    //Validación de campos con solo espacios
                    $obligatorios.forEach(el => {
                        if (el.value.trim() == "") {
                            el.classList.add('wrong_input');
                            flag = true;
                        }
                    });
                    //Cuando los campos no estan llenados correctamente mandamos excepción
                    if (flag) throw "Campos invalidos"
                    //Cuerpo de la petición
                    const request = new FormData();
                    //Datos en texto
                    const json = {
                        data:{
                            type:"events",
                            attributes:{
                                name:document.getElementById('name').value,
                                date:document.getElementById('date').value + ' ' + document.getElementById('time').value,
                                city:document.getElementById('city').value,
                                place:document.getElementById('place').value,
                                ticket_limit:document.getElementById('ticket_limit').value,
                                minium_age_to_pay:document.getElementById('minium_age_to_pay').value,
                                information:document.getElementById('information').value,
                                price:document.getElementById('price').value
                            }
                        }
                    }
                    //Insertando campos opcionales
                    $opcionales.forEach(el => {
                        if (el.value && el.value.trim() !== '' && el.id !== "discount") 
                            json.data.attributes[el.id] = el.value;
                        else if (el.id === "discount" && el.value > 0) {
                            json.data.attributes["status"] = "oferta";
                            json.data.attributes[el.id] = el.value+"%";
                        }
                    });
                    //Agregando información al cuerpo de la respuesta
                    request.append('json', JSON.stringify(json));
                    request.append('image', document.getElementById('poster').files[0]);
                    //Realizando la petción
                    ajax(
                        'POST',
                        'http://localhost:1000/api/v1/event',
                        request,
                        data => {
                            //Evento agregado
                            alert("Hecho!", `Evento ${data.data.attributes.name} agregado con éxito`, "success");
                            $form_create.reset();
                        },
                        error => {
                            //En caso de error
                            console.log(error);
                            alert('Error', error.errors[0].detail, 'danger');
                        },
                        {
                            'Authorization': localStorage.getItem('token')
                        }
                    );
                } catch (error) {
                    alert('Error', error, 'danger')   
                }          
            }
        });
        //Al cargar la página mostramos los eventos
        allEvents();
    }
})();