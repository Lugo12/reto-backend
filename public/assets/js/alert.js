//función para mostrar alerta personalizada
export default (cabecera, mensaje, tipo) => {
    const $alerta = document.getElementById('alerta');
    $alerta.innerHTML = `
     <div class="alert alert-${tipo}">
        <h4 class="alert-heading">${cabecera}</h4>
        ${mensaje}
    </div>
    `;
    $alerta.classList.remove('oculto'); //mostramos alerta
    setTimeout(() => {
        $alerta.classList.add('oculto');
        $alerta.innerHTML = "";
    }, 4000);  //desaparecemos alerta despues de 4 segundos
};