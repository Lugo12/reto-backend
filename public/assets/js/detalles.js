import ajax from "./ajax.js";   //importando la función aja
//Función autoinvocable
(function(){
    //Elemnto del DOM donde se alojara nuestro evento
    const $main = document.querySelector('main');
    //Recuperamos el id de la ruta
    const id = window.location.toString().split('/')[4];
    //Realizamos la petición al servidor
    ajax(
        'GET',
        `http://localhost:1000/api/v1/event/${id}`,
        null,
        data => {               
            //Atributos del evento
            const attributes = data.data.attributes;
            //Se pinta el evento
            $main.innerHTML = `
                <div class="detalles-evento">
                    <div class="titulo-evento">
                        <h2>${attributes.name}</h2>
                    </div>
                    <div class="detalles">
                        <div class="left-detail">
                            <p class="date">Fecha: ${attributes.date}</p>
                            <p class="place">Lugar: ${attributes.place}, ${attributes.city}</p>
                            <p class="ticket-limit">Límite de tickets: ${attributes.ticket_limit} por persona</p>
                            <p class="minium_age_to_pay">Pagan a partir de: ${attributes.minium_age_to_pay} años</p>
                            <p class="information">Información: ${attributes.information}</p>
                            <p class="price">Precio: $MXN${attributes.price}</p>
                            <p class="status">Estado: ${attributes.status}</p>
                            ${ (attributes.approximate_duration) ? `<p class="approximate_duration">Duración aproximada: ${attributes.approximate_duration}</p>`: '' }
                            ${ (attributes.age_limit) ? `<p class="age_limit">Límite de edad: ${attributes.age_limit}</p>` : '' }
                            ${ (attributes.restrictions) ? `<p class="restrictions">Restricciones: ${attributes.restrictions}</p>` : '' }
                            ${ (attributes.services) ? `<p class="services">Servicios: ${attributes.services}</p>` : '' }
                            ${ (attributes.official_website) ? `<p class="official_website">Stio web oficial: ${attributes.official_website}</p>` : '' }
                            ${ (attributes.discount) ? `<p class="discount">Descuento: ${attributes.discount}</p>` : '' }
                        </div>
                        <div class="right-detail">
                            <div class="imagen">
                                <img src="/assets/img/${attributes.poster}" alt="<image fail>">
                            </div>
                        </div>
                    </div>
                </div>
            `;
        },
        error => {
            console.error(error);
            $main.innerHTML = `<h2 class="errors">${error.errors[0].detail}</h2>`;
        }
    );
})();