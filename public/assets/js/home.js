import ajax from "./ajax.js";   //importando la función aja
//Función autoinvocable
(function(){
    //Elemnto del DOM donde se alojara nuestro evento
    const $main = document.querySelector('main');
    //Peticion para eventos próximos
    ajax(
        'GET',
        'http://localhost:1000/api/v1/event?status=próximo',
        null,
        data => {
            const total = data.meta.page.total;
            if (total > 0) {
                let $proximos = builder('proximos', 'Eventos próximos'); 
                $proximos = addEvnts(data.data, $proximos, (total >= 5) ? 5 : total);
                $main.prepend($proximos);   
            }                        
        },
        errors => {
            console.log(errors);
            error();
        }
    );
    //Peticion para eventos en oferta
    ajax(
        'GET',
        'http://localhost:1000/api/v1/event?status=oferta',
        null,
        data => {
            const total = data.meta.page.total;
            if (total > 0) {
                let $ofertas = builder('ofertas', 'Eventos en oferta');
                $ofertas = addEvnts(data.data, $ofertas, (total >= 5) ? 5 : total);
                ($main.childElementCount > 0) 
                ? $main.insertBefore($ofertas, $main.firstChild.nextSibling)
                : $main.appendChild($ofertas);
            }
        },
        errors => {
            console.log(errors);
            error();
        }
    );
    //Peticion para eventos pasados
    ajax(
        'GET',
        'http://localhost:1000/api/v1/event?status=pasado',
        null,
        data => {
            const total = data.meta.page.total;
            if (total > 0) {
                let $pasados = builder('pasados', 'Eventos pasados');
                $pasados = addEvnts(data.data, $pasados, (total >= 5) ? 5 : total);
                $main.append($pasados); 
            }
        },
        errors => {
            console.log(errors);
            error();
        }
    );
    //Mensaje en caso de error
    const error = () => {
        $main.innerHTML = `<h2 class="errors">Ocurrió un error al cargar los eventos</h2>`;
    };
    //Construcción del contenedor de eventos
    const builder = (status, title) => {
        //Divs
        const divMain = document.createElement('div'),
            divTitle = document.createElement('div'),
            divContent = document.createElement('div'),
            divFooter = document.createElement('div');
        //h2
        const h2Title = document.createElement('h2');
        //a
        const aFooter = document.createElement('a');
        //Clases
        divMain.classList.add(status);
        divTitle.classList.add('title_events');
        divContent.classList.add('content_events');
        divFooter.classList.add('footer_events');
        //href
        aFooter.href = `http://localhost:1000/${status}`;
        //innerHTML
        h2Title.innerHTML = title;
        aFooter.innerHTML = 'Ver más';
        //appendChild
        divTitle.appendChild(h2Title);
        divFooter.appendChild(aFooter);
        divMain.appendChild(divTitle);
        divMain.appendChild(divContent);
        divMain.appendChild(divFooter);

        return divMain;    
    };
    //Agregando eventos al contenedor
    const addEvnts = (data, $contenedor, total) => {
        for (let i = 0; i < total; i++) {
            const $event = document.createElement('div');
            $event.classList.add('event');
            $event.dataset.id = data[i].id;
            $event.innerHTML = `
                <div class="event_image">
                    <img src="assets/img/${data[i].attributes.poster}" alt="<image lost>">
                </div>
                <div class="event_details">
                    <p> ${data[i].attributes.place} </p>
                    <h4> ${data[i].attributes.name} </h4>
                </div>
            `;
            $contenedor.children[1].appendChild($event);
        }

        return $contenedor;
    };
})();