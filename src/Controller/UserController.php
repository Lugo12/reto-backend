<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Doctrine\DBAL\DriverManager;
use App\Services\JwtAuth;
use ErrorException;

class UserController extends AbstractController
{
    public function register(Request $request): Response
    {
        //Mensaje en caso de error
        $message = null;

        //Creamos una respuesta por defecto para el cliente
        $response = new Response(null, 400, [
            'Content-Type' => 'application/vnd.api+json'
        ]);

        //Recibir los datos por POST
        $json = $request->getContent();

        //Comprobar y validar datos
        if (!empty($json)) {
            try {
                //Decodificar el json 
                $data = json_decode($json)->data;

                $userName = (!empty($data->attributes->user)) ? $data->attributes->user : null;
                $email = (!empty($data->attributes->email)) ? $data->attributes->email : null;
                $password = (!empty($data->attributes->password)) ? $data->attributes->password : null;

                $validator = Validation::createValidator();
                $validate_email = $validator->validate($email, [
                    new Email()
                ]);

                if (
                    !empty($email) 
                    && count($validate_email) == 0 
                    && !empty($userName) 
                    && !empty($password)
                ) { 
                    //Comprobar si el usuario existe (duplicados)
                    $conn = DriverManager::getConnection(['url' => $_ENV['DATABASE_URL']]);
                    $queryBuilder = $conn->createQueryBuilder();

                    $isset_user = $queryBuilder
                        ->select('*')
                        ->from('users')
                        ->where('user = ? or email = ?')
                        ->setParameter(0, $userName)
                        ->setParameter(1, $email)
                        ->fetchAllAssociative()
                    ;

                    if (count($isset_user) > 0) {
                        $message = 'Usuario o correo existente';
                    } else {
                        //Cifrar contraseña
                        $pwd = sha1($password);

                        //Si no existe guardarlo en la db
                        $queryBuilder
                            ->insert('users')
                            ->values(
                                [
                                    'user' => '?',
                                    'email' => '?',
                                    'password' => '?'
                                ]
                            )
                            ->setParameter(0, $userName)
                            ->setParameter(1, $email)
                            ->setParameter(2, $pwd)
                            ->executeQuery()
                        ;

                        //Recuperamos el id del usuario registrado
                        $resultSet = $conn->executeQuery('SELECT * FROM users WHERE user = ?', [$userName]);
                        $id = strval($resultSet->fetchFirstColumn()[0]);
                                
                        //Creamos el cuerpo de la respuesta 
                        $body = [
                            'data' => [
                                'type' => 'users',
                                'id' =>  $id,
                                'attributes' => [
                                    'user' => $userName,
                                    'email' => $email
                                ]
                            ]
                        ];

                        $response->setStatusCode(201);
                    }
                } else {
                    $message = 'El usuario no se ha creado por campos invalidos';
                }
            } catch (ErrorException $e) {
                $message = 'El formato de la petición no es el correcto';
            }
        } else {
            $message = 'El usuario no se ha creado por falta de campos';
        }

        //Si se encontro algún error, se envia una respuesta con errores
        if ($message) {
            $body = [
                'errors' => [
                    [
                        'status' => '400',
                        'title' =>  'Bad Request',
                        'detail' => $message,
                        'source' => [ 'pointer' => '/api/v1/register' ]
                    ]                
                ]
            ];
        }

        //Establecemos el body final de la respuesta
        $response->setContent(json_encode($body));

        return $response;
    }

    public function login(Request $request, JwtAuth $jwtAuth): Response
    {
        //Mensaje en caso de error
        $message = null;

        //Creamos una respuesta por defecto para el cliente
        $response = new Response(null, 400, [
            'Content-Type' => 'application/vnd.api+json'
        ]);

        //Recibir los datos por POST
        $json = $request->getContent();

        //Comprobar y validar datos
        if (!empty($json)) {
            try {
                $data = json_decode($json)->data;
                $email = (!empty($data->attributes->email)) ? $data->attributes->email : null;
                $password = (!empty($data->attributes->password)) ? $data->attributes->password : null;
    
                $validator = Validation::createValidator();
                $validate_email = $validator->validate($email, [
                    new Email()
                ]);            
    
                if (
                    !empty($email) 
                    && count($validate_email) == 0 
                    && !empty($password) 
                ) {
                    //Cifrar contraseña
                    $pwd = sha1($password);
    
                    //Si todo es valido, identificamos al usuario con jwt
                    $body = $jwtAuth->signup($email, $pwd);
    
                    if (isset($body['data'])) {
                        $response->setStatusCode(200);
                    } else {
                        $message = $body;
                    }
                } else {
                    $message = 'Los campos son invalidos';
                }    
            } catch (ErrorException $e) {
                $message = 'El formato de la petición no es el correcto';
            }
        } else {
            $message = 'Fallo la autenticación debido a falta de campos';
        }

        //Si se encontro algún error, se envia una respuesta con errores
        if ($message) {
            $body = [
                'errors' => [
                    [
                        'status' => '400',
                        'title' =>  'Bad Request',
                        'detail' => $message,
                        'source' => [ 'pointer' => '/api/v1/login' ]
                    ]                
                ]
            ];
        }

        //Establecemos el body final de la respuesta
        $response->setContent(json_encode($body));

        return $response;
    }
    //Validador de tokens
    public function isValid(Request $request, JwtAuth $jwtAuth): Response
    {
        //Recogemos los datos por POST y lo decodificamos
        $token = json_decode($request->getContent())->token;
        //Cuerpo de la respuesta
        $body = ['is_valid' => $jwtAuth->checkToken($token)];
         
        return new Response(json_encode($body), 200, [
            'Content-Type' => 'application/vnd.api+json'
        ]);
    } 
}