<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class VistaController extends AbstractController
{
    //Render de la pantalla de registro
    public function registro(): Response
    {
        return $this->render('register/register.html.twig');
    }
    //Render de la pantalla de login
    public function signIn(): Response
    {
        return $this->render('login/login.html.twig');
    }
    //Render de la pantalla de inicio
    public function index(): Response
    {
        return $this->render('event/index.html.twig');
    }
    //Render de la pantalla de detalles del evento
    public function detalles(): Response
    {
        return $this->render('event/evento.html.twig');
    }
    //Render de la pantalla de eventos próximos
    public function proximos(): Response
    {
        return $this->render('event/proximos.html.twig');
    }
    //Render de la pantalla de eventos pasados
    public function pasados(): Response
    {
        return $this->render('event/pasados.html.twig');
    }
    //Render de la pantalla de eventos en oferta
    public function ofertas(): Response
    {
        return $this->render('event/ofertas.html.twig');
    }
    //Render de la pantalla del CRUD del admin
    public function admin(): Response
    {
        return $this->render('admin/admin.html.twig');
    }
}
