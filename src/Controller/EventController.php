<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Exception\DriverException;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\DBAL\DriverManager;
use App\Services\JwtAuth;
use ErrorException;
use Exception;
use TypeError;

class EventController extends AbstractController
{
    public function createEvent(Request $request, JwtAuth $jwtAuth): Response
    {
        //Mensaje en caso de error
        $message = null;

        //Recoger la cabecera de autenticación
        $token = $request->headers->get('Authorization');

        //Creamos una respuesta por defecto para el cliente
        $response = new Response(null, 400, [
            'Content-Type' => 'application/vnd.api+json'
        ]);

        //Comprobamos si se envio un token
        if (!empty($token)) {
            $authCheck = $jwtAuth->checkToken($token, true);

            //Validamos si el token es correcto
            if ($authCheck) {
                //Recibir los datos por POST
                $json = $request->request->get('json');
                $image = $request->files->get('image');

                //Comprobar y validar datos
                if ($json && $image) {
                    try {
                        //Validamos que la imagen se haya subido correctamente
                        if (!$image->isValid()) {
                            throw new Exception('Ocurrio un error durante la carga de la imagen');
                        }
                        
                        $mimeType = $image->getClientMimeType();

                        //Validamos el formato de las imagenes
                        if (
                            $mimeType != "image/jpeg"
                            && $mimeType != "image/png"
                            && $mimeType != "image/gif"
                        ) {
                            throw new Exception('Solo se permiten archivos con formato (jpeg, png y gif)');
                        }                        

                        //Decodificar el json y obtenemos los atributos de la imagen
                        $values = (array) json_decode($json)->data->attributes;

                        //Intentamos insertar los valores en la base de datos
                        try {
                            /* Convertimos los string a int en los atributos que
                            lo requieran y los validamos */
                            foreach ($values as $key => $value) {
                                if ($value && !empty($value)) {
                                    if ($key == "ticket_limit" || $key == "minium_age_to_pay") {
                                        $values[$key] = (is_numeric($value) && intval($value) > 0) ? intval($value) : null;
                                    } elseif ($key == "price") {
                                        $values[$key] = (is_numeric($value) && floatval($value) > 0) ? floatval($value) : null;
                                    } elseif ($key == "discount") {
                                        /* Validación para el correcto formato
                                        del descuento */
                                        $descuento = explode('%', $value);
                                        $porcentaje = substr($value, -1, 1);

                                        if (
                                            !is_numeric($descuento[0]) 
                                            || intval($descuento[0]) <= 0
                                            || $porcentaje != "%"
                                        ) {
                                            throw new Exception('El valor del descuento debe ser mayor a 0 y menor a 100, con el siguiente fortmato de ejemplo (50%)');
                                        }
                                    } else if ($key == "date") {
                                        //Validamos el formato de la fecha y la hora
                                        $fecha = substr($values['date'], 0, 10);
                                        $hora = substr($values['date'], 11, 5);
                                        $Ymd = explode('-', $fecha);
                                        $Hi = explode(':', $hora);

                                        if (
                                            strlen($values['date']) != 16
                                            || count($Ymd) != 3 
                                            || count($Hi) != 2
                                            || !checkdate($Ymd[1], $Ymd[2], $Ymd[0]) 
                                            || ($Hi[0] < 0 || $Hi[0] > 23)
                                            || ($Hi[1] < 0 || $Hi[1] > 59)
                                        ) {
                                            throw new Exception('El formato de la fecha y hora no es el correcto (Y-m-d H:i)');
                                        }

                                        //Validación de la fecha 
                                        date_default_timezone_set("America/mexico_city"); 
                                        $hoy = date('Y-m-d H:i');

                                        if ($value <= $hoy) {
                                            throw new Exception('No se permite guardar eventos con fechas pasadas');
                                        }
                                    }
                                } else {
                                    throw new Exception('Los valores de los datos no pueden ser nulos o vacios');
                                }                        
                            }

                            //Nombre de la imagen en la base de datos y el servidor
                            $image_name = time()."_".$image->getClientOriginalName();

                            //Insertamos el evento en la base de datos
                            $conn = DriverManager::getConnection(['url' => $_ENV["DATABASE_URL"]]);
                            $queryBuilder = $conn->createQueryBuilder();

                            $queryBuilder
                                ->insert('events')
                                ->values(
                                    [
                                        'user_id' => '?',
                                        'name' => '?',
                                        'date' => '?',
                                        'city' => '?',
                                        'place' => '?',
                                        'ticket_limit' => '?',
                                        'minium_age_to_pay' => '?',
                                        'information' => '?',
                                        'poster' => '?',
                                        'price' => '?',
                                        'status' => '?',
                                        'approximate_duration' => '?',
                                        'age_limit' => '?',
                                        'restrictions' => '?',
                                        'services' => '?',
                                        'official_website' => '?',
                                        'discount' => '?'
                                    ]
                                )
                                ->setParameter(0, $authCheck->sub)
                                ->setParameter(1, $values['name'])
                                ->setParameter(2, $values['date'])
                                ->setParameter(3, $values['city'])
                                ->setParameter(4, $values['place'])
                                ->setParameter(5, $values['ticket_limit'])
                                ->setParameter(6, $values['minium_age_to_pay'])
                                ->setParameter(7, $values['information'])
                                ->setParameter(8, $image_name)
                                ->setParameter(9, $values['price'])
                                ->setParameter(10, (
                                        isset($values['status'])
                                        && $values['status'] == 'oferta'
                                    ) ? $values['status'] : 'próximo'
                                )
                                ->setParameter(11, (
                                        isset($values['approximate_duration'])
                                    ) ? $values['approximate_duration'] : null
                                )
                                ->setParameter(12, (
                                        isset($values['age_limit'])
                                    ) ? $values['age_limit'] : 'Todas las Edades'
                                )
                                ->setParameter(13, (
                                        isset($values['restrictions'])
                                    ) ? $values['restrictions'] : null
                                )
                                ->setParameter(14, (
                                        isset($values['services'])
                                    ) ? $values['services'] : null
                                )
                                ->setParameter(15, (
                                        isset($values['official_website'])
                                    ) ? $values['official_website'] : null
                                )
                                ->setParameter(16, (
                                        isset($values['discount'])
                                        && isset($values['status'])
                                        && $values['status'] == 'oferta'
                                    ) ? $values['discount'] : null
                                )
                                ->executeQuery()
                            ;

                            //Guardamos la imagen en el servidor
                            $image->move("../public/assets/img",$image_name);

                            //Obtenemos el id del evento insertado
                            $id = $conn->executeQuery("SELECT LAST_INSERT_ID()")->fetchOne();
                            
                            //Datos del evento recien ingresado
                            $event = $conn->executeQuery("SELECT * FROM events WHERE id = ?", [$id])->fetchAssociative();

                            //Cuerpo de la respuesta
                            $body = [                                
                                'data' => [
                                    'type' => 'events',
                                    'id' => strval($id),
                                    'attributes' => [
                                        'user_id' => $authCheck->sub
                                    ]
                                ],
                                'links' => [
                                    'self' => '/api/v1/event/'.$id
                                ]
                            ];

                            /*Por cada atributo del evento que no sea nulo lo insertamos a los
                            cuerpo de la respuesta*/
                            foreach ($event as $key => $value) {
                                if ($value && $key != 'id') {
                                    $body['data']['attributes'][$key] = strval($value);
                                }
                            }

                            $response->setStatusCode(201);   
                        } catch (TypeError $e) {
                            $message = 'La fecha solo puede estar compuesta por enteros y (-)';
                        } catch (ErrorException $e) {
                            $message = 'Los datos del evento no estan completos o no son los correctos';
                        } catch (DriverException $e) {
                            $message = 'Los datos enviados no cumplen con las caracteristicas establecidas';
                        } catch (Exception $e) {
                            $message = $e->getMessage();
                        }
                    } catch (ErrorException $e) {
                        $message = 'El formato de la petición no es el correcto';
                    } catch (Exception $e) {
                        $message = $e->getMessage();
                    } 
                } else {
                    $message = 'Los datos del evento son necesarios (json, image)';
                }
            } else {
                $message = 'El token no es valido';
            }
        } else {
            $message = 'Necesita un token de autenticacón para crear eventos';
        }

        //Si se encontro algún error, se envia una respuesta con errores
        if ($message) {
            $body = [
                'errors' => [
                    [
                        'status' => '400',
                        'title' =>  'Bad Request',
                        'detail' => $message,
                        'source' => [ 'pointer' => '/api/v1/event' ]
                    ]                
                ]
            ];
        }

        //Establecemos el body final de la respuesta
        $response->setContent(json_encode($body));

        return $response;
    }

    // FUNCIÓN PARA LISTAR TODOS LOS EVENTOS CON PAGINACIÓN
    public function events(Request $request, PaginatorInterface $paginator): Response
    {
        try {
            //Mensaje en caso de error
            $message = null;

            //Creamos una respuesta por defecto para el cliente
            $response = new Response(null, 400, [
                'Content-Type' => 'application/vnd.api+json'
            ]);

            //Status solicitado en la petición
            $status = $request->query->get('status', null);

            //Validando el status enviado
            if (
                $status != "próximo"
                && $status != "oferta"
                && $status != "pasado"
                && $status != null
            ) {
                throw new Exception('El parámetro status solo puede ser (próximo, oferta o pasado)');
            }

            //Hacemos una consulta para traer todos los eventos
            $conn = DriverManager::getConnection(['url' => $_ENV['DATABASE_URL']]);
            $queryBuilder = $conn->createQueryBuilder();

            //Variable para definir el parametro de status en las URL
            $statusP = "";

            if ($status) {
                $events = $this->updateStatus(
                    $queryBuilder
                    ->select('*')
                    ->from('events')
                    ->where('status = ?')
                    ->setParameter(0, $status)            
                    ->fetchAllAssociative(),
                    false,
                    $status)
                ;
                //Odenamos los eventos
                if ($status != 'pasado') {
                    //Ordenamiento ascendente
                    usort($events, function($a, $b){
                        return strcmp($a['date'], $b['date']);
                    });
                } else {
                    //Ordenamiento descendente
                    usort($events, function($a, $b){
                        return strcmp($b['date'], $a['date']);
                    });
                }
                //Actualizamos la variable que va en las URL de la respuesta
                $statusP = "status=".$status."&";
            } else {
                $events = $this->updateStatus(
                    $queryBuilder
                    ->select('*')
                    ->from('events')        
                    ->fetchAllAssociative())
                ;
            }

            //Dandole formato a la respuesta
            foreach ($events as $key => $value) {
                //Esqueleto de la data
                $event = [
                    'type' => 'events',
                    'id' => $value["id"],
                    'attributes' => [],
                    'relationships' => [
                        'author' => [
                            'data' => [ 'type' => 'users', 'id' => strval($value['user_id']) ]
                        ]
                    ]
                ];

                //Si el atributo del evento no es nulo o el id lo agregamos
                foreach ($value as $llave => $valor) {
                    if ($valor && $llave != 'id') {
                        $event['attributes'][$llave] = strval($valor);
                    }
                }

                //Reemplazamos el contenido por nuestra evento personalizado
                $events[$key] = $event;
            }        

            //Pagina enviada en la petición
            $page = $request->query->getInt('page', 1);     

            //Eventos por página
            $itemsPerPage = 10;     

            //Paginación
            $pagination = $paginator->paginate(
                $events, 
                $page,
                $itemsPerPage
            );

            //Variables del Meta y los links
            $data = $pagination->getItems();
            $total = $pagination->getTotalItemCount();
            $total_pages = ceil($total / $itemsPerPage);
            $prev = ($page - 1 == 0) ? $page : $page - 1;
            $next = ($page + 1 > $total_pages) ? $page : $page + 1;
            $from = (isset($pagination->getItems()[0])) ? $pagination->getItems()[0]["id"] : 0;
            $to = (isset(end($data)["id"])) ? end($data)["id"] : 0;

            //Reiniciamos el cuerpo de la respuesta y le insertamos los eventos
            $body = [
                'meta' => [
                    'page' => [
                        'current-page' => $page,
                        'per-page' => $itemsPerPage,
                        'from' => $from,
                        'to' => $to,
                        'total' => $total,
                        'last-page' => $total_pages
                    ]
                ]
            ];
            //Si no existen eventos con este estaus no agregamos links
            if ($total != 0) {
                $body['links'] = [
                    'first' => 'http://localhost:1000/api/v1/event?'.$statusP.'page=1',
                    'prev' => 'http://localhost:1000/api/v1/event?'.$statusP.'page='.$prev,
                    'next' => 'http://localhost:1000/api/v1/event?'.$statusP.'page='.$next,
                    'last' => 'http://localhost:1000/api/v1/event?'.$statusP.'page='.$total_pages
                ]; 
            }
            //Agregamos la data
            $body['data'] = $data;
            //Si se solicita la información del cliente
            $include = $request->query->get('include', null);
            if ($include === "user") {
                $body['included'] = [];
                //Por cada evento buscamos a su creador/actualizador
                foreach ($data as $event) {
                    //Id del usuario
                    $id_user = $event['relationships']['author']['data']['id'];
                    //Bandera para determinar repetición
                    $exist = false;
                    //Validamos que el usuario ya este agregado a la respuesta
                    foreach ($body['included'] as $author) {
                        if ($id_user === $author['id']) {
                            $exist = true;
                        }
                    }
                    //Si no existe lo agregamos
                    if (!$exist) {
                        //Nombre del usuario
                        $user_name = $conn
                            ->executeQuery(
                                'SELECT user FROM users WHERE id = ?',
                                [$id_user],
                            )
                            ->fetchOne()
                        ;
                        //Información del usuario en el cuerpo de la respuesta
                        $user = [
                            'type' => 'users',
                            'id' => $id_user,
                            'attributes' => [
                              'user' => $user_name
                            ]
                        ];
                        //Agregamos al usuario a la respuesta
                        array_push($body['included'], $user);
                    }
                }
            }
            //Status de la respuesta
            $response->setStatusCode(200);
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        //Si se encontro algún error, se envia una respuesta con errores
        if ($message) {
            $body = [
                'errors' => [
                    [
                        'status' => '400',
                        'title' =>  'Bad Request',
                        'detail' => $message,
                        'source' => [ 'pointer' => '/api/v1/event' ]
                    ]                
                ]
            ];
        }

        //Establecemos el body final de la respuesta
        $response->setContent(json_encode($body));

        return $response;
    }

    // FUNCIÓN PARA MOSTRAR UN EVENTO CON SUS DETALLES POR ID
    public function event($id): Response
    {
        //Mensaje en caso de error
        $message = null;

        //Creamos una respuesta por defecto para el cliente
        $response = new Response(null, 400, [
            'Content-Type' => 'application/vnd.api+json'
        ]);

        try {
            //Validamos que el evento exista
            $conn = DriverManager::getConnection(['url' => $_ENV['DATABASE_URL']]);
            $queryBuilder = $conn->createQueryBuilder();

            $isset_event = $queryBuilder
                ->select('*')
                ->from('events')
                ->where('id = ?')
                ->setParameter(0, $id)
                ->fetchAssociative()
            ;

            //Si existe actualizamos su status y creamos el cuerpo de la respuesta
            if ($isset_event) {
                $event = $this->updateStatus($isset_event, true);

                //Obtenemos el usuario que creo el evento
                $user = $conn
                    ->executeQuery('SELECT user FROM users Where id = ?', [$event['user_id']])
                    ->fetchOne()
                ;

                //Cuerpo de la respuesta
                $body = [
                    'data' => [
                        'type' => 'events',
                        'id' => strval($event['id']),
                        'attributes' => [],
                        'relationships' => [
                            'author' => [
                                'data' => [ 'type' => 'users', 'id' => strval($event['user_id']) ]
                            ]
                        ]
                    ],
                    'included' => [
                        [
                            'type' => 'users',
                            'id' => strval($event['user_id']),
                            'attributes' => [
                                'user' => $user
                            ]
                        ]
                    ]
                ];

                /*Por cada atributo del evento que no sea nulo lo insertamos a los
                cuerpo de la respuesta*/
                foreach ($event as $key => $value) {
                    if ($value && $key != 'id') {
                        $body['data']['attributes'][$key] = strval($value);
                    }
                }

                $response->setStatusCode(200);
            } else {
                $message = 'Este evento no existe';
            }
        } catch (DriverException $e) {
            $message = 'Algo salio mal, intentelo de nuevo más tarde';
        }

        //Si se encontro algún error, se envia una respuesta con errores
        if ($message) {
            $body = [
                'errors' => [
                    [
                        'status' => '400',
                        'title' =>  'Bad Request',
                        'detail' => $message,
                        'source' => [ 'pointer' => '/api/v1/event/'.$id ]
                    ]                
                ]
            ];
        }

        //Establecemos el body final de la respuesta
        $response->setContent(json_encode($body));

        return $response;
    }

    // FUNCIÓN PARA ACTUALIZAR LOS DETALLES DE UN EVENTO
    public function updateEvent(Request $request, JwtAuth $jwtAuth, $id): Response
    {
        //Mensaje en caso de error
        $message = null;

        //Recoger la cabecera de autenticación
        $token = $request->headers->get('Authorization');

        //Creamos una respuesta por defecto para el cliente
        $response = new Response(null, 400, [
            'Content-Type' => 'application/vnd.api+json'
        ]);

        //Comprobamos si se envio un token
        if (!empty($token)) {
            $authCheck = $jwtAuth->checkToken($token, true);

            //Validamos si el token es correcto
            if ($authCheck) {
                //Recibir los datos por POST
                $json = $request->get('json');
                $image = $request->files->get('image');

                try {                    
                    if ($json || $image) {
                        //Arreglo donde se almacenarán los datos a actualizar
                        $newEvent = [];
                        //Conexión a la base de datos
                        $conn = DriverManager::getConnection(['url' => $_ENV["DATABASE_URL"]]);
                        
                        //Si se actualizan los datos
                        if ($json) {
                            //Decodificar el json y obtenemos los atributos de la evento
                            $newEvent = (array) json_decode($json)->data->attributes;

                            //Array de llaves permitidas
                            $keysAllowed = [
                                'name',
                                'date',
                                'city',
                                'place',
                                'ticket_limit',
                                'minium_age_to_pay',
                                'information',
                                'price',
                                'status',
                                'approximate_duration',
                                'age_limit',
                                'restrictions',
                                'services',
                                'official_website',
                                'discount',
                            ];

                            //Validamos los datos de la petición
                            foreach ($newEvent as $key => $value) {                            
                                //Validamos que los atributos sean los correctos
                                if (!in_array($key, $keysAllowed)) {
                                    unset($newEvent[$key]);
                                }

                                //Campos vacios o nulos
                                if (!$value || empty($value)) {
                                    throw new Exception('Los valores de los datos no pueden ser nulos o vacios');
                                }                            

                                //Validando datos númericos
                                if (
                                    ($key == "ticket_limit" 
                                    || $key == "minium_age_to_pay" 
                                    || $key == "price") 
                                    && !is_numeric($value)
                                ) {
                                    throw new Exception('Solo valores númericos para (ticket_limit, minium_age_to_pay y price)');
                                }

                                //Validación de status
                                if (
                                    $key == "status" 
                                    && $value != "próximo"
                                    && $value != "oferta"
                                ) {
                                    throw new Exception('El status solo puede ser modificado a próximo u oferta');
                                }

                                //Validación para el correcto formato del descuento
                                if ($key == "discount") {                                
                                    $descuento = explode('%', $value);
                                    $porcentaje = substr($value, -1, 1);

                                    if (
                                        !is_numeric($descuento[0]) 
                                        || intval($descuento[0]) <= 0
                                        || $porcentaje != "%"
                                    ) {
                                        throw new Exception('El valor del descuento debe ser mayor a 0 y menor a 100, con el siguiente fortmato de ejemplo (50%)');
                                    }                                    

                                    $newEvent["status"] = "oferta";
                                }

                                //Actualizamos el status a próximo y retiramos la oferta
                                if (
                                    $key == "status"
                                    && $value == "próximo"
                                ) {
                                    $newEvent["discount"] = null;
                                }

                                //Validación para la fecha
                                if ($key == "date") {                                
                                    //Validamos el formato de la fecha y la hora
                                    $fecha = substr($newEvent['date'], 0, 10);
                                    $hora = substr($newEvent['date'], 11, 5);
                                    $Ymd = explode('-', $fecha);
                                    $Hi = explode(':', $hora);

                                    if (
                                        strlen($newEvent['date']) != 16
                                        || count($Ymd) != 3 
                                        || count($Hi) != 2
                                        || !checkdate($Ymd[1], $Ymd[2], $Ymd[0]) 
                                        || ($Hi[0] < 0 || $Hi[0] > 23)
                                        || ($Hi[1] < 0 || $Hi[1] > 59)
                                    ) {
                                        throw new Exception('El formato de la fecha y hora no es el correcto (Y-m-d H:i)');
                                    }

                                    //Validamos que la fecha sea futura
                                    date_default_timezone_set("America/mexico_city"); 
                                    $hoy = date('Y-m-d H:i');

                                    if ($value <= $hoy) {
                                        throw new Exception('No se permite actualizar eventos con fechas pasadas');
                                    }

                                    //status actual
                                    $currentStatus = $conn
                                        ->executeQuery(
                                            'SELECT status FROM events Where id = ?',
                                            [$id]
                                        )
                                        ->fetchOne()
                                    ;

                                    //Tiene descuento ?
                                    $currentDiscount = $conn
                                        ->executeQuery(
                                            'SELECT discount FROM events Where id = ?',
                                            [$id]
                                        )
                                        ->fetchOne()
                                    ;

                                    //Actualización de eventos pasados
                                    if (
                                        $currentStatus === 'pasado' 
                                        && !isset($newEvent["discount"])
                                        && !$currentDiscount
                                    ) {
                                        $newEvent["status"] = 'próximo';
                                    } else if (
                                        $currentStatus === 'pasado' 
                                        && (
                                            isset($newEvent["discount"])
                                            || $currentDiscount
                                        )
                                    ) {
                                        $newEvent["status"] = 'oferta';
                                    }
                                }
                            }
                        }                                            
                        
                        //Actualizamos la imagen en caso de que se tenga que hacer
                        if ($image) {
                            //Validamos que la imagen se haya subido correctamente
                            if (!$image->isValid()) {
                                throw new Exception('Ocurrio un error durante la carga de la imagen');
                            }

                            //Validamos el formato de las imagenes
                            $mimeType = $image->getClientMimeType();

                            if (
                                $mimeType != "image/jpeg"
                                && $mimeType != "image/png"
                                && $mimeType != "image/gif"
                            ) {
                                throw new Exception('Solo se permiten archivos con formato (jpeg, png y gif)');
                            }  

                            //Nombre de la imagen en la base de datos y el servidor
                            $newPoster = time()."_".$image->getClientOriginalName();
                
                            /* Asignamos el nombre del poster para que se guarde
                            la base de datos */
                            $newEvent["poster"] = $newPoster;

                            /* Recuperamos el nombre del viejo poster para
                            eliminarlo posteriormente si todo sale bien */
                            $oldPoster = $conn
                                ->executeQuery(
                                    "SELECT poster FROM events WHERE id = ?",
                                    [$id]
                                )
                                ->fetchOne()
                            ;
                        }

                        //Asignamos el id del usuario que esta editando el evento
                        $newEvent = array("user_id" => $authCheck->sub) + $newEvent;

                        //Actualizamos los datos en la base de datos
                        foreach ($newEvent as $key => $value) {
                            $conn
                                ->executeQuery(
                                    "UPDATE events set ".$key." = ? WHERE id = ?",
                                    [$value, $id]
                                );

                            /* Guardamos la imagen en el servidor en el momento
                            que se guarde en la base de datos */
                            if ($key == "poster") {
                                //Guardamos la imagen en el servidor
                                $image->move("../public/assets/img",$newPoster);
                                //Borramos la imagen anterior
                                unlink("../public/assets/img/".$oldPoster);
                            }
                        }

                        //Establecemos la respuesta en caso de éxito
                        $body = null;
                        $response->setStatusCode(204);
                    } else {
                        $message = 'Los datos del evento que se van a actualizar son necesarios';
                    }
                } catch (TypeError $e) {
                    $message = 'La fecha solo puede estar compuesta por enteros y (-)';
                } catch (ErrorException $e) {
                    $message = 'El formato de la petición no es el correcto';
                } catch (DriverException $e) {
                    $message = 'Los datos enviados no cumplen con las caracteristicas establecidas';
                } catch (Exception $e) {
                    $message = $e->getMessage();
                }
            } else {
                $message = 'El token no es valido';
            }
        } else {
            $message = 'Necesita un token de autenticacón para actualizar eventos';
        }

        //Si se encontro algún error, se envia una respuesta con errores
        if ($message) {
            $body = [
                'errors' => [
                    [
                        'status' => '400',
                        'title' =>  'Bad Request',
                        'detail' => $message,
                        'source' => [ 'pointer' => '/api/v1/event/'.$id ]
                    ]                
                ]
            ];
        }

        //Establecemos el body final de la respuesta
        $response->setContent(json_encode($body));

        return $response;
    }

    // FUNCIÓN PARA BORRAR UN EVENTO POR ID
    public function deleteEvent(Request $request, JwtAuth $jwtAuth, $id): Response
    {
        //Mensaje en caso de error
        $message = null;

        //Recoger la cabecera de autenticación
        $token = $request->headers->get('Authorization');

        //Creamos una respuesta por defecto para el cliente
        $response = new Response(null, 400, [
            'Content-Type' => 'application/vnd.api+json'
        ]);

        //Comprobamos si se envio un token
        if (!empty($token)) {
            $authCheck = $jwtAuth->checkToken($token);

            //Validamos si el token es correcto
            if ($authCheck) {
                //Comprobamos si el evento existe
                $conn = DriverManager::getConnection(['url' => $_ENV['DATABASE_URL']]);
                $queryBuilder = $conn->createQueryBuilder();

                $isset_event = $queryBuilder
                    ->select('*')
                    ->from('events')
                    ->where('id = ?')
                    ->setParameter(0, $id)
                    ->fetchAssociative()  
                ;
    
                //Si existe borramos el evento
                if ($isset_event) {
                    /* Recuperamos el nombre del viejo poster para
                    eliminarlo posteriormente si todo sale bien */
                    $oldPoster = $conn
                        ->executeQuery(
                            "SELECT poster FROM events WHERE id = ?",
                            [$id]
                        )
                        ->fetchOne()
                    ;

                    //Borramos el evento de la base de datos
                    $queryBuilder
                        ->delete('events')
                        ->where('id = ?')
                        ->setParameter(0, $id)
                        ->executeQuery()
                    ;                    

                    //Borramos la imagen del evento en el servidor
                    unlink("../public/assets/img/".$oldPoster);

                    $body = null;
                    $response->setStatusCode(204);
                } else {
                    $message = 'El evento no existe';                       
                }
            } else {
                $message = 'El token no es valido';
            }
        } else {
            $message = 'Necesita un token de autenticacón para borrar eventos';
        }

        //Si se encontro algún error, se envia una respuesta con errores
        if ($message) {
            $body = [
                'errors' => [
                    [
                        'status' => '400',
                        'title' =>  'Bad Request',
                        'detail' => $message,
                        'source' => [ 'pointer' => '/api/v1/event/'.$id ]
                    ]                
                ]
            ];
        }

        //Establecemos el body final de la respuesta
        $response->setContent(json_encode($body));

        return $response;
    }

    // FUNCIÓN PARA ACTUALIZAR LOS EVENTOS QUE YA PASARON
    private function updateStatus($events, $one = false, $status = null)
    {
        $conn = DriverManager::getConnection(['url' => $_ENV['DATABASE_URL']]);
        $queryBuilder = $conn->createQueryBuilder();
        
        //Establecemos la fecha actual
        date_default_timezone_set("America/mexico_city"); 
        $hoy = date('Y-m-d H:i');
        
        if (!$one) {
            /*Por cada evento con fecha pasada y estado diferente a pasado, se le
            actualiza el estatus*/
            foreach ($events as $event) {
                if ($event["date"] <= $hoy && $event["status"] != "pasado"){
                    $conn
                        ->executeQuery(
                            "UPDATE events SET status = ? WHERE id = ?",
                            ["pasado", $event["id"]]
                        );                    
                }
            }

            //regresamos los eventos actualizados al día
            if ($status) {
                $res = $conn
                    ->executeQuery('SELECT * FROM events WHERE status = ?', [$status])
                    ->fetchAllAssociative()
                ;
            } else {
                $res = $conn
                    ->executeQuery('SELECT * FROM events')
                    ->fetchAllAssociative()
                ;
            }            
        } else {
            if ($events["date"] <= $hoy && $events["status"] != "pasado"){
                $queryBuilder
                    ->update('events', 'e')
                    ->set('e.status', '?')
                    ->where('e.id = ?')
                    ->setParameter(0, 'pasado')
                    ->setParameter(1, $events["id"])
                    ->executeQuery()
                ;
            }

            //regresamos el evento actualizados al día
            $res = $conn
                ->executeQuery('SELECT * FROM events WHERE id = ?', [$events["id"]])
                ->fetchAssociative()
            ;
        }
        
        return $res;
    }
}
