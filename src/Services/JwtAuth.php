<?php

namespace App\Services;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Doctrine\DBAL\DriverManager;

class JwtAuth 
{
    public $key;

    public function __construct()
    {
        $this->key = $_ENV['APP_SECRET'];
    }

    public function signup($email, $password)
    {
        //Comprobar si el usuario existe
        $conn = DriverManager::getConnection(['url' => $_ENV['DATABASE_URL']]);
        $queryBuilder = $conn->createQueryBuilder();

        $isset_user = $queryBuilder
            ->select('*')
            ->from('users')
            ->where('email = ? and password = ?')
            ->setParameter(0, $email)
            ->setParameter(1, $password)
            ->fetchAssociative()
        ;

        //Si existe, generar el token de jwt
        if ($isset_user) {
            $payload = [
                'sub' => $isset_user['id'],
                'user' => $isset_user['user'],
                'email' => $isset_user['email'],
                'iat' => time(),
                'exp' => time() + (2 * 60 * 60)     //Duración del token: 2h
            ];
 
            $jwt = JWT::encode($payload, $this->key, 'HS256');

            $body = [
                'data' => [
                    'type' => 'users',
                    'id' => strval($isset_user['id']),
                    'attributes' => [
                        'user' => $isset_user['user'],
                        'email' => $isset_user['email'],
                        'token' => $jwt
                    ]
                ]
            ];

        } else {
            $body = 'Credenciales invalidas';
        }

        //devolver datos
        return $body;
    }    

    public function checkToken($jwt, $return = false) 
    {
        $auth = true;

        try {
            $decoded = JWT::decode($jwt, new Key($this->key, 'HS256'));
            if ($return) {
                $auth = $decoded;
            }
        } catch (\DomainException $e) {
            $auth = false;
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        }

        return $auth;
    }
}