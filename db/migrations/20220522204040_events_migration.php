<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

final class EventsMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $events = $this->table('events', ['signed' => false]);
        
        $events
        ->addColumn('user_id', 'integer')
        ->addColumn('name', 'string', ['limit' => 150])
        ->addColumn('date', 'datetime')
        ->addColumn('city', 'string', ['limit' => 150])
        ->addColumn('place', 'string', ['limit' => 150])
        ->addColumn('ticket_limit', 'integer', [
            'limit' => MysqlAdapter::INT_TINY
            ])
        ->addColumn('minium_age_to_pay', 'integer', [
            'limit' => MysqlAdapter::INT_TINY
            ])
        ->addColumn('information', 'text')
        ->addColumn('poster', 'string', ['limit' => 150])
        ->addColumn('price', 'float')
        ->addColumn('status', 'enum', [
            'values' => ['próximo', 'pasado', 'oferta'],
            'default' => 'próximo',
            'null' => true
            ])
        ->addColumn('approximate_duration', 'string', [
            'limit' => 100, 
            'null' => true
            ])
        ->addColumn('age_limit', 'string', [
            'limit' => 100,
            'default' => 'Todas las Edades',
            'null' => true
            ])
        ->addColumn('restrictions', 'string', ['limit' => 300, 'null' => true])
        ->addColumn('services', 'string', ['limit' => 300, 'null' => true])
        ->addColumn('official_website', 'string', [
            'limit' => 100, 
            'null' => true
            ])
        ->addColumn('discount', 'char', ['limit' => 3, 'null' => true])
        ->addForeignKey('user_id', 'users', 'id', [
            'delete'=> 'NO_ACTION', 
            'update'=> 'NO_ACTION', 
            'constraint' => 'fk_event_user'
            ])
        ->create();
    }
}
