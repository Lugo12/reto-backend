<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InsertsEMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(): void
    {
        $events = $this->table('events');

        $events->insert([
            [
                'user_id' => 1,
                'name' => 'Disney On Ice Celebremos',
                'date' => "2022-07-05 20:30",
                'city' => 'Ciudad de México, DF',
                'place' => 'Auditorio Nacional',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 2,
                'information' => 'Ponte tus orejas de Mickey y prepárate para la máxima fiesta de Disney On Ice Celebremos.  Acompaña a 50 de tus amigos favoritos de Disney que tomarán vida en esta increíble aventura sobre hielo con patinadores de clase mundial. Con los anfitriones Mickey y Minnie, lánzate a la aventura a través de 14 de las historias más grandiosas de Disney jamás contadas. Canta al ritmo de Olaf mientras sueña con el verano, comparte el asombro de Forky cuando aprende lo que significa ser un juguete y olvida tus preocupaciones al lado de Timón y Pumba. Sueña en grande con las valientes Princesas de Disney y mucho más mientras vas creando nuevos recuerdos con toda tu familia en Disney On Ice Celebremos.',
                'poster' => '1653321517_disney.jpg',
                'price' => 1499.99
            ],
            [
                'user_id' => 1,
                'name' => 'Interpol Gradas',
                'date' => "2022-06-28 20:00",
                'city' => 'México, DF',
                'place' => 'Palacio de los Deportes',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => "La banda liderada por el carismático Paul Banks y los músicos Sam Fogarino y Daniel Kessler, Interpol, regresará a nuestro país para ofrecer un energético concierto. Para esta nueva visita, los creadores de hits como Obstacle 1, Evil, C'Mere, Slow hands, The heinrich maneuver y Rest my chemistry llegarán de la mano del que será su más reciente disco, que aún no tiene nombre, y que será el séptimo en su cuenta. La cita pactada para esta poderosa velada musical será el sábado, 28 de mayo de 2022 en el Palacio de los Deportes. La preventa Citibanamex se realizará los días 26 y 27 de julio.",
                'poster' => '1653321518_interpol.jpg',
                'price' => 1668,
                'approximate_duration' => '120 min aproximadamente',
                'restrictions' => 'Prohibido Cámaras con lente desmontable, ingresar con Alimentos y Bebidas, Mochilas.',
                'services' => 'Bar, Snacks, Souvenirs oficiales'
            ],
            [
                'user_id' => 1,
                'name' => 'Justin Bieber',
                'date' => "2022-06-25 20:30",
                'city' => 'México, DF',
                'place' => 'Foro Sol',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Justin Bieber anunció hoy sus nuevas fechas para su muy esperado Justice World Tour, donde incluye la Ciudad de México. La cita será el 25 de mayo de 2022 en el Foro Sol.  El tour se llevará a cabo desde mayo de 2022 hasta marzo de 2023. Bieber recorrerá los cinco continentes, viajará a más de 20 países y realizará más de 90 fechas, con presentaciones adicionales en Japón, Asia y Medio Oriente que se anunciarán muy pronto. Justice World Tour, promovido por AEG Presents, es la primera salida global de Justin desde el Purpose World Tour de 2016 y 2017. Descrito por The Times of London como "fascinante", Bieber tocó ante más de 2 millones de fans durante esos años para finalizar con un aforo de 65 millones de seguidores en el festival británico Summer Time Hyde Park de Londres.',
                'poster' => '1653321519_justin.jpg',
                'price' => 3000,
                'approximate_duration' => '2 hrs',
                'restrictions' => 'No se permite el acceso con alimentos, bebidas, cámaras fotográficas y/o vídeo, armas, mascotas',
                'services' => 'venta de souvenir, alimentos, bebidas'
            ],
            [
                'user_id' => 1,
                'name' => 'Café Tacvba',
                'date' => "2022-06-18 20:30",
                'city' => 'Ciudad de México, DF',
                'place' => 'Auditorio Nacional',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Café Tacvba es un estandarte del movimiento musical y cultural de nuestro país y en toda Latinoamérica. Ha sido parte del soundtrack de muchas generaciones y su música no solo ha traspasado fronteras, sino que ha encontrado la forma de arraigarse en otros países que jamás se hubieran imaginado. Después de un alto total en sus actividades y con un resurgimiento de los grandes conciertos masivos, Café Tacvba sabe que el momento de bailar y rockear en compañía de sus fans ha llegado. En junio, la banda se presentará con su segundo desconectado en el Coloso de Reforma, un concepto cuyo material fue grabado en la sala Nezahualcóyotl de la UNAM ante 2000 invitados, convirtiendo a Café Tacvba en la primera agrupación latinoamericana en grabar un segundo concierto en este formato.',
                'poster' => '1653321520_cafe.jpg',
                'price' => 1600,
                'approximate_duration' => '2 Horas',
                'restrictions' => 'Se recomienda llegar con anticipación',
                'services' => 'Estacionamiento, dulcería y bar.'
            ],
            [
                'user_id' => 1,
                'name' => 'Maluma Arena VFG',
                'date' => "2022-06-20 20:30",
                'city' => 'Tlajomulco de Zuñiga, JAL',
                'place' => 'Arena V.F.G.',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 4,
                'information' => '¡El artista latino más internacional regresa a México con su espectacular show! En 2022, el cantante urbano latino con mayor impacto en el mundo, MALUMA, recorrerá varios escenarios de nuestro país con su Papi Juancho Maluma Tour. El colombiano visitará las siguientes ciudades: 20 de mayo, Querétaro (Próximamente a la venta) 4 de junio, Guadalajara, Arena VFG 11 de junio, CDMX, Palacio de Deportes Y más shows por anunciar…',
                'poster' => '1653321521_maluma.jpg',
                'price' => 1600,
                'approximate_duration' => '2 horas.',
                'restrictions' => 'Menores de 4 años no ingresan al inmueble, favor de seguir las indicaciones de Salud',
                'services' => 'Venta de alimentos y bebidas.'
            ],
            [
                'user_id' => 1,
                'name' => 'Iron Maiden',
                'date' => "2022-09-07 20:30",
                'city' => 'México, DF',
                'place' => 'Foro Sol',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Después de sus tres impactantes fechas agotadas en la Ciudad de México en 2019, Bruce Dickinson, Steve Harris, Janick Gears, Dave Murray, Nicko McBrain y Adrian Smith, llegarán de nueva cuenta a nuestro país con este poderoso y asombroso espectáculo, que ha sido visto por casi dos millones de personas en todo el mundo.',
                'poster' => '1653321522_ironmaiden.jpg',
                'price' => 1960,
                'approximate_duration' => '2 hrs',
                'restrictions' => 'No se permite el acceso con alimentos, bebidas, cámaras fotográficas y/o vídeo, armas, mascotas',
                'services' => 'Venta de alimentos, bebidas, souvenir'
            ],
            [
                'user_id' => 1,
                'name' => 'Dua Lipa',
                'date' => "2022-09-21 20:30",
                'city' => 'México, DF',
                'place' => 'Foro Sol',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Como parte de su esperada gira mundial Future Nostalgia Tour, la superestrella Dua Lipa llegará a México el 21 de septiembre de 2022 para ofrecer una noche de fiesta en el Foro Sol de la Ciudad de México junto a sus más grandes fans mexicanos. Future Nostalgia Tour es la cuarta gira oficial de conciertos de la estrella pop Dua Lipa, quien en 2022 pisará varios países de Latinoamérica como Argentina, Colombia y, por supuesto, México.',
                'poster' => '1653321523_dualipa.jpg',
                'price' => 1560,
                'approximate_duration' => '2 horas',
                'restrictions' => 'No se permite el acceso con alimentos, bebidas, cámaras fotográficas y/o vídeo, armas, mascotas',
                'services' => 'Venta de alimentos, bebidas, souvenir'
            ],
            [
                'user_id' => 1,
                'name' => 'Sonora Dinamita de Lucho Argaín',
                'date' => "2022-09-25 18:30",
                'city' => 'Ciudad de México, DF',
                'place' => 'Auditorio Nacional',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Llega La Sonora Dinamita de Lucho Argain al Auditorio Nacional a festejar 60 años de historia, un show único donde podrás escuchar y bailar los principales éxitos de su carrera como "Que Bello", "El Viejo del Sombrerón", "Escándalo", "La Parabólica" y muchos más, habrá grandes invitados, ¡No te lo puedes perder!',
                'poster' => '1653321524_sonora.jpg',
                'price' => 980,
                'approximate_duration' => '2 Horas',
                'restrictions' => 'No se permite el acceso con alimentos, bebidas, cámaras fotográficas o de vídeo',
                'services' => 'Estacionamiento, zona de alimentos y bebidas.',
                'official_website' => 'www.auditorio.com.mx'
            ],
            [
                'user_id' => 1,
                'name' => 'Rammstein Amerika Stadium Tour Feuerzone GA',
                'date' => "2022-11-01 20:30",
                'city' => 'México, DF',
                'place' => 'Foro Sol',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'RAMMSTEIN conformado por CHRISTOPH SCHNEIDER, FLAKE LORENZ, OLIVER RIEDEL, PAUL LANDERS, RICHARD Z. KRUSPE, y TILL LINDEMANN, se ha mantenido sin cambios desde la formación de la banda en 1994 y el lanzamiento del disco debut HERZELEID en 1995. El segundo álbum de RAMMSTEIN, SEHNSUCHT, lanzado en 1997, impulsó a la banda a la fama internacional y presentó el sencillo nominado al GRAMMY DU HAST. MUTTER, el tercer álbum de RAMMSTEIN, fue lanzado en 2001 y fue apoyado, por lo que sería, la gira final de la banda por Estados Unidos durante casi una década. REISE, REISE, el cuarto álbum de estudio de la banda, fue lanzado en 2004, precedido por el single nominado al GRAMMY MEIN TEIL y seguido por el quinto álbum ROSENROT en 2005. LIEBE IST FÜR ALLE DA, el sexto álbum de estudio RAMMSTEIN, fue lanzado en 2009 y alcanzó el puesto número 13 en el Billboard 200, lo que lo convierte en el álbum con más éxitos de RAMMSTEIN en Estados Unidos, hasta ese momento.',
                'poster' => '1653321525_ramnstein.jpg',
                'price' => 2000,
                'approximate_duration' => '2 Horas.',
                'restrictions' => 'No se permite el acceso con alimentos, bebidas, cámaras fotográficas y/o vídeo, armas, mascotas',
                'services' => 'Venta de alimentos, bebidas, souvenir'
            ],
            [
                'user_id' => 1,
                'name' => "Bad Bunny World's Hottest Tour",
                'date' => "2022-12-09 20:30",
                'city' => 'México, DF',
                'place' => 'Estadio Azteca',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => '¡Pellízcate! Porque esto no es un sueño ya que el World´s Hottest Tour de Bad Bunny llega al Estadio Azteca este próximo 9 de diciembre.',
                'poster' => '1653321526_badbunny.jpg',
                'price' => 6000,
                'approximate_duration' => '2.30 horas aproximadamente.',
                'restrictions' => 'Use cubrebocas, mantenga la sana distancia, no podrá ingresar si presenta temperatura mayor a 37.7°C o tiene enfermedad respiratoria.',
                'services' => 'Venta de Alimentos y Bebidas.',
                'official_website' => 'http//www.worldshottesttour.com'
            ],
            [
                'user_id' => 1,
                'name' => "Racing Bike Mexico Gp400",
                'date' => "2022-06-26 11:00",
                'city' => 'México, DF',
                'place' => 'Autódromo Hermanos Rodríguez',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 12,
                'information' => 'Ven a festejar con nosotros nuestro décimo aniversario Racing Bike Mexico GP400, presentado por El Universal. Niños menores de 11 años no pagan boleto',
                'poster' => '1653321527_racingbike.jpg',
                'price' => 360,
                'approximate_duration' => '4 Horas',
                'restrictions' => 'No se permite el ingreso con alimentos,bebidas,cámaras fotográficas y/o video,armas,mascotas.',
                'services' => 'Venta de alimentos, bebidas, souvenirs'
            ],
            [
                'user_id' => 1,
                'name' => "Lux Fight League 022",
                'date' => "2022-06-18 18:00",
                'city' => 'México, DF',
                'place' => 'Pepsi Center WTC',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Amantes del box este el es momento del año que estaban esperando, no se pierdan este evento, en su vida volveran a ver algo igual',
                'poster' => '1653321528_luxfight.jpg',
                'price' => 300,
                'restrictions' => 'Por disposición oficial no se permite el acceso al recinto con cámaras de bolsillo y/o cámaras profesionales.',
                'services' => 'A&B en general'
            ],
            [
                'user_id' => 1,
                'name' => "Daddy Yankee",
                'date' => "2022-05-22 20:00",
                'city' => 'México, DF',
                'place' => 'Foro Sol',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Máximo exponente y pionero del reggaetón, Daddy Yankee asombró al mundo al anunciar las fechas de su gira de despedida La Última Vuelta y su último álbum Legendaddy, con un emotivo mensaje a sus fans a través de su página web, donde expresó su retiro de la industria de la música después de 32 años de trayectoria, 6 discos de estudio, más de 30 millones de discos vendidos, más de 102 premios ganados y un legado de fans que consolidaron el género como uno de los más importantes en el mundo. "Formalmente hoy anuncio mi retiro de la música, entregándoles mi mejor producción y mi mejor gira de conciertos y lo voy a despedir celebrando estos 32 años de trayectoria con esta pieza de colección titulada Legendaddy. Les voy a dar todos los estilos que me han definido en un solo álbum. Legendaddy es lucha, es fiesta, es guerra, romance".',
                'poster' => '1653321529_daddy.jpg',
                'price' => 3000,
                'status' => 'pasado',
                'approximate_duration' => '2 horas',
                'restrictions' => 'No se permite el acceso con alimentos, bebidas, cámaras fotográficas y/o vídeo, armas, mascotas',
                'services' => 'Venta de alimentos, bebidas, souvenir'
            ],
            [
                'user_id' => 1,
                'name' => "Danny Ocean",
                'date' => "2022-05-21 20:00",
                'city' => 'Ciudad de México, DF',
                'place' => 'Auditorio Nacional',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Este 2022 nadie se rehúsa a una buena fiesta con uno de los artistas más calientes del momento: desde Caracas, llegará Danny Ocean a uno de los escenarios más imponentes de todo el mundo, el del Auditorio Nacional. Guarda bien la fecha, porque el próximo 15 de octubre el cantante y productor encenderá las butacas de este gran recinto para armar una fiestota que no te vas a querer perder.',
                'poster' => '1653321530_dannyO.jpg',
                'price' => 1500,
                'status' => 'pasado',
                'approximate_duration' => '2 Horas',
                'official_website' => 'www.auditorio.com.mx'
            ],
            [
                'user_id' => 1,
                'name' => "Ernesto D'Alessio y grandes invitados",
                'date' => "2022-07-26 20:30",
                'city' => 'México, DF',
                'place' => 'Teatro Metropólitan',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => "Ernesto D'alessio está más que listo para regresar al escenario musical y hacer vibrar el Teatro Metropolitan y ha preparado para esta ocasión un show que disfrutarás de principio a fin, con banda en vivo, mariachi y grandes artistas invitados. Celebrando 25 años de carrera y heredero de una gran voz, compartirá escenario con la más grande interprete de América Latina su madre: ¡Lupita D'alessio! donde será inevitable que se te ponga la piel chinita.",
                'poster' => '1653321531_ernesto.jpg',
                'price' => 400,
                'status' => 'oferta',
                'approximate_duration' => '2 h.',
                'restrictions' => 'Se recomienda llegar con anticipación',
                'discount' => '10%'
            ],
            [
                'user_id' => 1,
                'name' => "Network",
                'date' => "2022-05-24 17:30",
                'city' => 'México, DF',
                'place' => 'Teatro de los Insurgentes',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 16,
                'information' => 'Tras 25 años trabajando en una cadena de TV, el veterano presentador Howard Beale debe abandonar su puesto por el bajo nivel de audiencia. …Beale, viudo y alcohólico, queda trastocado y decide anunciar durante una emisión que se suicidará en su último programa  ante las cámaras. Basada en la galardonada película de 1976 y tras su exitosa temporada en el National Theater de Londres y en el Belasco Theater de Broadway, Network es un acido análisis sobre el poder de la televisión, los medios, los gobiernos y las grandes corporaciones en un mundo competitivo donde  el éxito y los récords de audiencia imponen su dictadura',
                'poster' => '1653321532_network.jpg',
                'price' => 570,
                'status' => 'pasado',
                'approximate_duration' => 'Una hora, 40 minutos',
                'restrictions' => 'No se permite la entrada con alimentos, armas, cámaras fotográficas o de video, así como el uso de celulares durante la función',
                'services' => 'Se cuenta con cafetería, bar, dulcería y valet parking',
                'official_website' => 'www.network-mexico.com'
            ], 
            [
                'user_id' => 1,
                'name' => "Lucero & Mijares",
                'date' => "2022-05-20 20:30",
                'city' => 'Ciudad de México, DF',
                'place' => 'Auditorio Nacional',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Lucero y Mijares juntos nuevamente como nunca los habias visto',
                'poster' => '1653321533_L&M.jpg',
                'price' => 600,
                'status' => 'pasado',
                'approximate_duration' => '2 horas.',
                'restrictions' => 'No se permite el acceso con alimentos, bebidas, cámaras fotográficas o de vídeo',
                'services' => 'Estacionamiento, dulcería, zona de snacks y bebidas'
            ],
            [
                'user_id' => 1,
                'name' => "Aladdín, El Éxito de Broadway",
                'date' => "2022-05-19 20:00",
                'city' => 'México, DF',
                'place' => 'Teatro Telcel',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'El gran éxito de Broadway ahora en México',
                'poster' => '1653321534_aladin.jpg',
                'price' => 1628.25,
                'status' => 'pasado'                
            ], 
            [
                'user_id' => 1,
                'name' => "Jungle",
                'date' => "2022-05-26 11:30",
                'city' => 'México, DF',
                'place' => 'Pepsi Center WTC',
                'ticket_limit' => 8,                
                'minium_age_to_pay' => 3,
                'information' => 'Jungle es un colectivo musical británico de música electrónica, conformado en Londres',
                'poster' => '1653321535_jungle.jpg',
                'price' => 3000
            ]
        ])->saveData();
    }

    public function down(): void
    {
        $this->execute('DELETE FROM events');
    }
}
