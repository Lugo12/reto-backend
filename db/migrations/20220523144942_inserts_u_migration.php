<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class InsertsUMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up(): void
    {
        $users = $this->table('users');

        $users->insert(
            [
                'user' => 'Lugo1207',
                'email' => 'luisagm0507@gmail.com',
                'password' => '3641b5f8b04ef07a3defa37b19411c63f900c3da'
            ]
        )->saveData();
    }

    public function down(): void
    {
        $this->execute('DELETE FROM users');
    }
}
