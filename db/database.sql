CREATE DATABASE IF NOT EXISTS db_reto_backend;
USE db_reto_backend;

CREATE TABLE IF NOT EXISTS users(
    id INT NOT NULL AUTO_INCREMENT,
    user VARCHAR(70) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    CONSTRAINT pk_users PRIMARY KEY(id) 
);

CREATE TABLE IF NOT EXISTS events(
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    name VARCHAR(150) NOT NULL,
    date DATETIME NOT NULL,
    city VARCHAR(150) NOT NULL,
    place VARCHAR(150) NOT NULL,
    ticket_limit TINYINT NOT NULL,
    minium_age_to_pay TINYINT NOT NULL,
    information TEXT NOT NULL,
    poster VARCHAR(150) NOT NULL,
    price FLOAT NOT NULL,
    status enum('próximo', 'pasado', 'oferta') default 'próximo',
    approximate_duration VARCHAR(100) NULL,
    age_limit VARCHAR(100) default 'Todas las Edades' NULL,
    restrictions VARCHAR(300) NULL,
    services VARCHAR(300) NULL,
    official_website VARCHAR(100) NULL,
    discount CHAR(3) NULL,
    CONSTRAINT pk_events PRIMARY KEY(id),
    CONSTRAINT fk_event_user FOREIGN KEY(user_id) REFERENCES users(id) 
);