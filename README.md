# Reto backend - Eventmaster
### __Requisitos__
- Docker
- Docker compose
- Mysql (cualquier versión)
---
### __Comandos útiles__
- __make init__: Compila y corre el entorno completo por primera vez
  (contenedores, servidor, librerías y migraciones)  
- __make run__: Inicia el servidor de desarrollo de Symfony
- __make start__: Inicia los contenedores y el servidor
- __make migrate__: Ejecuta las migraciones de phinx
- __make help__: Muestra todos los comndos y su descripción
---
### __Instrucciones__
1. Una vez clonado el repositorio, abrir el proyecto en una terminal y usar el
   comando _make init_ __sólo la primera vez__
2. Para ver el sitio web, ir al navegador e ingresar al puerto __1000__ del
   _localhost_ ó dar click en el enlace del homepage en la sección [Sitio web -
   rutas](#sitio-web)
3. Para correr la aplicación por segunda o más veces, usar el comando _make start_

Notas:

- En caso de que el servidor no se haya iniciado y no se pueda visualizar el sitio, usar el comando _make run_

- Si por alguna razón las migraciones no se ejecturaron de forma correcta y en
  el sitio se muestra el mensaje ___Ocurrió un error al cargar los eventos___, usar el comando _make migrate_

---
### __Api__
#### Endpoints
| Evento | Endpoint | Método HTTP | Controlador | Descripción |
| --- | --- | --- | --- | --- |
| Register | http://localhost:1000/api/v1/register | POST | User | Registra un nuevo usuario |
| Login | http://localhost:1000/api/v1/login | POST | User | Valida los datos del usuario y devuelve un token de autenticación |
| Create | http://localhost:1000/api/v1/event | POST | Event | Crea un nuevo evento (Requiere token de autenticación) |
| Read | http://localhost:1000/api/v1/event | GET | Event | Devuelve la información de todos los eventos con paginación |
| Update | http://localhost:1000/api/v1/event/{id} | POST | Event | Actualiza un evento existente (Requiere token de autenticación) |
| Delete | http://localhost:1000/api/v1/event/{id} | DELETE | Event | Elimina un evento por id (Requiere token de autenticación) |
| Read by ID | http://localhost:1000/api/v1/event/{id} | GET | Event | Devuelve la información específica de un evento |
| Is token | http://localhost:1000/api/v1/token | POST | User | Valida que el token enviado sea correcto y no haya expirado ya |

#### Postman
___Register___
- Request
    - Headers

    ![fail-load](capture/postman/registro_header_request.png "request header")

    - Body

    ![fail-load](capture/postman/registro_body_request.png "request body")

- Response

![fail-load](capture/postman/registro_response.png "response")

___Login___
- Request    
    - Headers

    ![fail-load](capture/postman/login_header_request.png "request header")

    - Body

    ![fail-load](capture/postman/login_body_request.png "request body")

- Response

![fail-load](capture/postman/login_response.png "response")

___Create event___
- Request    
    - Headers

    ![fail-load](capture/postman/create_header_request.png "request header")

    - Body

    ![fail-load](capture/postman/create_body_request.png "request body")

- Response

![fail-load](capture/postman/create_response_1.png "response")
![fail-load](capture/postman/create_response_2.png "response")

___Read events___
- Request    
    - Headers

    ![fail-load](capture/postman/read_header_request.png "request header")

- Response
    - Default

    ![fail-load](capture/postman/read_response_1.png "response by default")
    
    - Data

    ![fail-load](capture/postman/read_response_2.png "data")
    ![fail-load](capture/postman/read_response_3.png "data")

    La data es un arreglo de eventos con su respectiva información, además
    incluye la relación con el usuario que lo creó/actualizó
        
    - Parámetros
        - page

        ![fail-load](capture/postman/read_params_1.png "page")

        Recibe el número de página que se quiere consultar

        - status

        ![fail-load](capture/postman/read_params_2.png "status")

        Cuando se especifíca, devuelve todos los eventos correspondientes al
        valor dado. Valores posibles: _próximo_, _oferta_ y _pasado_
        
        - include

        ![fail-load](capture/postman/read_params_3.png "include")
        
        Cuando su valor es _user_, este devuelve un objeto extra que contiene la
        información de todos los usuarios relacionados con los eventos que se
        encuentran en la página actual

___Update event___
- Request    
    - Headers

    ![fail-load](capture/postman/update_header_request.png "request header")

    - Body

    ![fail-load](capture/postman/update_body_request.png "request body")
    
- Response

![fail-load](capture/postman/update_response.png "response")

No se retorna ninguna información en la respuesta, el status es 204 _No Content_

___Delete event___
- Request    
    - Headers

    ![fail-load](capture/postman/delete_header_request.png "request header")

- Response

    ![fail-load](capture/postman/delete_response.png "response")
    
    No se retorna ninguna información en la respuesta, el status es 204 _No Content_

___Event by ID___
- Request
    - Headers

    ![fail-load](capture/postman/eventbyid_header_request.png "request header")

- Response

    ![fail-load](capture/postman/eventbyid_response_1.png "response")
    ![fail-load](capture/postman/eventbyid_response_2.png "response")

___Is token___
- Request
    - Headers

    ![fail-load](capture/postman/token_header_request.png "request header")

    - Body

    ![fail-load](capture/postman/token_body_request.png "request body")
    
- Response

![fail-load](capture/postman/token_response.png "response")

---
### __Sitio web__
#### Rutas
- Home page: http://localhost:1000/

![fail-load](capture/home-page.png "homepage")

- Eventos próximos: http://localhost:1000/proximos

![fail-load](capture/proximos-page.png "próximos")

- Eventos en oferta: http://localhost:1000/ofertas

![fail-load](capture/oferta-page.png "ofertas")

- Eventos pasados: http://localhost:1000/pasados

![fail-load](capture/pasados-page.png "pasados")

- Login: http://localhost:1000/sign-in

![fail-load](capture/login-page.png "login")

- Registro: http://localhost:1000/registro

![fail-load](capture/registro-page.png "registro")

- Admin (solo si se esta logueado): http://localhost:1000/admin

![fail-load](capture/admin-page.png "admin")

---
### __Issues__
- [Issues](https://gitlab.com/Lugo12/reto-backend/-/issues)
- [Issue Board](https://gitlab.com/Lugo12/reto-backend/-/boards)
---
### __Librerías usadas__
_Librerías solicitadas_

- __Symfony Routing__: Librería usada para el tema de las rutas en las funciones
  de los controladores
- __Doctrine Database Abstraction Layer__: Librería empleada para las consultas a la base de datos
- __Symfony Http Kernel__: Utilice completamente Symfony http foundation
- __Symfony Http Foundation__: Fue muy útil para manejar las peticiones y enviar
  respuestas en las funciones de la api
- __Twig__: La ocupe para hacer mis plantillas del sitio web
- __Phinx.org__: Fué muy útil para hacer migraciones de tablas y datos

_Librerías extras_

- __symfony/maker-bundle__: facilitó la creación de controladores en un inicio, además de que crea por
      defecto una plantilla para el controlador
- __firebase/php-jwt__: librería usada para la autenticación con tokens
- __symfony/asset__: permitió enlazar archivos js y css a las plantillas
- __knplabs/knp-paginator-bundle__: librería usada para la paginación de los eventos 
- __symfony/validator__: librería ocupada para validar la información enviada al api