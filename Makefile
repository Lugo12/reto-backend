#!/bin/bash

UID = $(shell id -u)
DOCKER_BE = php81-symfony54

help: ## Muestra este mensaje de ayuda
	@echo 'usage: make [target]'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

start: ## Inicia los contenedores
	docker network create php81-symfony54-network || true
	U_ID=${UID} docker-compose up -d && $(MAKE) run

stop: ## Detener los contenedores
	U_ID=${UID} docker-compose stop

restart: ## Reiniciar los contenedores
	$(MAKE) stop && $(MAKE) start

build: ## Construir todos los contenedores
	docker network create php81-symfony54_network || true
	U_ID=${UID} docker-compose build

run: ## Inicia el servidor de desarrollo de Symfony
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} symfony serve -d

logs: ## Muestra los logs de Symfony en tiempo real
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} symfony server:log

# Comandos backend
composer-install: ## Instala dependencias de composer
	U_ID=${UID} docker exec --user ${UID} ${DOCKER_BE} composer install --no-interaction
# Fin comandos backend

ssh-be: ## bash dentro del contenedor
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} bash

code-style: ## Corre php-cs para corregir el estilo de código siguiendo las reglas de Symfony
	U_ID=${UID} docker exec --user ${UID} ${DOCKER_BE} php-cs-fixer fix src --rules=@Symfony

migrate: ## Ejecuta las migraciones de phinx
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} vendor/bin/phinx migrate

mysql: ## bash dentro del contenedor de mysql
	docker exec -it php81-symfony54-mysql bash

init: ## Compila y corre el entorno completo por primera vez
	$(MAKE) start && $(MAKE) composer-install && $(MAKE) migrate